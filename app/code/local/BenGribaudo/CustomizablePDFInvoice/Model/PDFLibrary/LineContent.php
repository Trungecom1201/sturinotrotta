<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_LineContent extends BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	private $thickness;
	
	public function __construct($thickness) { 
		$this->thickness = $thickness;
	}
	
	protected function getContentHeight() {
		return $this->thickness;
	}
	
	protected function RenderContent($page, $xStart, $xEnd, $y) {
		$y -= ($this->thickness / 2);
		
		$page->saveGS();
		$page->setFillColor(new Zend_Pdf_Color_Rgb(1, 0, 1));
		$page->setLineWidth($this->thickness);
		$page->drawLine($xStart, $y, $xEnd, $y);
		$page->restoreGS();
	}
}