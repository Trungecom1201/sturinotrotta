<?php
/**
* BSS Commerce Co.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://bsscommerce.com/Bss-Commerce-License.txt
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento COMMUNITY edition
* BSS Commerce does not guarantee correct work of this extension
* on any other Magento edition except Magento COMMUNITY edition.
* BSS Commerce does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   BSS
* @package    Bss_DeleteOrder
* @author     Extension Team
* @copyright  Copyright (c) 2014-2105 BSS Commerce Co. ( http://bsscommerce.com )
* @license    http://bsscommerce.com/Bss-Commerce-License.txt
*/
class Bss_DeleteOrder_Model_Observer
{
    public function addMassAction($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction
            && $block->getRequest()->getControllerName() == 'sales_order')
        {
            $block->addItem('deleteorder', array(
                'label' => 'Delete Orders',
                'url' => Mage::helper("adminhtml")->getUrl('adminhtml/deleteorder_delete/order'),
                'confirm' => 'Are you sure ?',
                ));
        }

        if($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction
            && $block->getRequest()->getControllerName() == 'sales_invoice')
        {
            $block->addItem('deleteorder', array(
                'label' => 'Delete Invoices',
                'url' => Mage::helper("adminhtml")->getUrl('adminhtml/deleteorder_delete/invoice'),
                'confirm' => 'Delete Invoices... Are you sure?',
                ));
            $block->addItem('cancelinvoice', array(
                'label' => 'Cancel Invoices',  
                'url' => Mage::helper("adminhtml")->getUrl('adminhtml/deleteorder_delete/cancelinvoice'),
                'confirm' => 'Cancel Invoices... Are you sure?',
                ));
        }

        if($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction
            && $block->getRequest()->getControllerName() == 'sales_shipment')
        {
            $block->addItem('deleteorder', array(
                'label' => 'Delete Shipments',
                'url' => Mage::helper("adminhtml")->getUrl('adminhtml/deleteorder_delete/shipment'),
                'confirm' => 'Are you sure ?',
                ));
        }

        if($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction
            && $block->getRequest()->getControllerName() == 'sales_creditmemo')
        {
            $block->addItem('deleteorder', array(
                'label' => 'Delete Credit Memos',
                'url' => Mage::helper("adminhtml")->getUrl('adminhtml/deleteorder_delete/creditmemo'),
                'confirm' => 'Are you sure ?',
                ));
        }
    }
}