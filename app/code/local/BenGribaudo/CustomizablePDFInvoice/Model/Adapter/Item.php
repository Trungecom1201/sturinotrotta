<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Adapter_Item extends BenGribaudo_CustomizablePDFInvoice_Model_Adapter_Abstract {
	private $item, $orderItem, $renderer, $magentoPdfHelper;
	
	public function __construct($item) {
		$this->item = $item;
		// If we have difficulty because preAdapt isn't called before getting the invoice, it will be necessary to pass the invoice in.
		$this->invoice = $item->getInvoice();
		
		$this->preAdapt();
		$this->orderItem = $item->getOrderItem();
		$this->order = $this->invoice->getOrder();
		
		$this->magentoPdfHelper = new BenGribaudo_CustomizablePDFInvoice_Helper_MagentoPdf();
		$this->renderer = $this->magentoPdfHelper->getRenderer($this->orderItem->getProductType());
		$this->postAdapt();
	}
	
	public static function AdaptItems($invoice) {
		$output = array();
	
		// If we have difficulty because preAdapt isn't called before getting the items, it will be necessary to pass the invoice in.
		foreach ($invoice->getAllItems() as $item){
			if ($item->getOrderItem()->getParentItem()) {
				continue;
			}
			
			$adapter = new BenGribaudo_CustomizablePDFInvoice_Model_Adapter_Item($item);
			$output = array_merge($output, $adapter->adapt());
		}
		
		return $output;
	}
	
	public function adapt() {
		$this->preAdapt();
	
		$dataCapturer = new BenGribaudo_CustomizablePDFInvoice_Helper_DataCapturer();
		$parser = $this->getParser($this->realItemRenderType());
		
		$this->renderer->setOrder($this->order);
		$this->renderer->setItem($this->item);
		$this->renderer->setPdf($dataCapturer);
		$this->renderer->setPage(new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4));
		$this->renderer->setRenderedModel($dataCapturer);
		$this->renderer->draw();
		
		$baseWorkingData = $dataCapturer->_getDraw();
		$type = $this->realItemRenderType();
		
		$output = array();
		foreach ($baseWorkingData as $key => $value) {
			$parserOutput = $parser->adapt($value);
			foreach ($parserOutput as $k => &$v) {
				$v['type'] = $type;
			}
			$output = array_merge($output, $parserOutput);
		}
		
		$this->postAdapt();
		
		return $output;
	}
	
	private function realItemRenderType() {
		$type = get_class($this->renderer);
		
		if ($type == 'Mage_Sales_Model_Order_Pdf_Items_Invoice_Grouped') {
			$type = get_class($this->magentoPdfHelper->getRenderer($this->orderItem->getRealProductType()));
		}
		
		return $type;
	}
	
	private function getParser($type) {
		switch ($type) {
			case 'Mage_Sales_Model_Order_Pdf_Items_Invoice_Default':
				return new BenGribaudo_CustomizablePDFInvoice_Model_Adapter_ItemDataCapturers_Default();
			case 'Mage_Bundle_Model_Sales_Order_Pdf_Items_Invoice':
				return new BenGribaudo_CustomizablePDFInvoice_Model_Adapter_ItemDataCapturers_Bundle();
		}
	}
}