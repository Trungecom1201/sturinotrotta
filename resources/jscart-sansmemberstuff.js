/**
 * @author Jason Jaeger
 */

/*
var memberDiscounts = [];
memberDiscounts.silver = 15;
memberDiscounts.gold = 20;
memberDiscounts.collector = 25;
memberDiscounts.collectorPlus = 30;
*/

window.addEvent('domready', function(){
	
	//Add Event Handlers to Shipping Fields (except state)
	var shippingFields = [$('txt_shippingName'),
		($('txt_shipping_Street') ||null),
		($('txt_shippingCity') || null),
		($('txt_shippingZip') || null),
		($('txt_shippingPhone')||null),
		($('txt_shippingEmail'||null))
		]
		shippingFields.each(function(item,index){
			if(!item){return;}
			item.addEvents({
				'change':function(e){
					summarizeOrder($('ddl_billingState').value,$('ddl_shippingState').value);
				}
			})
		});
	
	
	var temp = getUrlVars();
	if(temp.product){	highlight(temp.product);	}
	if(temp.id){ updateCart(); }


	/*var accordion = new Accordion('div.toggler', 'div.toggled', {
		opacity: false,
		alwaysHide: true,
		display:false,
		transition:Fx.Transitions.Back.easeInOut,
		onActive: function(toggler, element){
			//toggler.setStyle('color', '#FF7000');
			toggler.setHTML ('Click Here to Hide Discount Information');
		},
	 
		onBackground: function(toggler, element){
			//toggler.setStyle('color', '#423E25');
			toggler.setHTML ('Click Here to Show Discount Information');
		}
	}, $('discountBlurbs'));*/


});

function highlight(id){
	if(id && xDOM(id)){
		var obj = xDOM(id);
		if(obj.className != 'productTable'){
			obj = findParentByClass(obj,'productTable');
			if(obj){ 
				obj.className = "highlighted";	
			}
		}
	}                
}

function findParentByClass(obj, className){
	if(obj && className){
		while( obj && obj.className != className ){ obj = obj.parentNode;	}
		if(obj){ return obj; }else{ return false;}
	}else{ return false;}
}
			
function swapImg(thisObj,newSrc){
thisObj.src = newSrc;
}



function add2Cellar(thisObj){
	updateCart();
	//get productName --------------
	var productName = thisObj.id;
	//alert(productName);
	//get quantity -----------------
	//alert(thisObj.previousSibling);
	var quantityObj = thisObj.previousSibling.previousSibling;
	
	if(quantityObj.className != 'qtyField'){
		quantityObj = quantityObj.previousSibling;
	}
	if(quantityObj.className != 'qtyField'){
		quantityObj = quantityObj.previousSibling;
	}
	if(quantityObj.value && IsNumeric(quantityObj.value)){
		//alert('requested Quantity: '+quantityObj.value);
		var requestedQuantity = quantityObj.value;
		var currentQuantity = 0;
		if(readCookie(productName+'-Q')){	currentQuantity = readCookie(productName+'-Q');	}
		var newQuantity = parseFloat(currentQuantity) + parseFloat(requestedQuantity);
		createCookie(productName+'-Q',newQuantity,1);
		//alert('new quantity: '+newQuantity);
	}else if(!quantityObj.value){
		alert('Please enter a quantity.');
	}else{
		alert('Please enter only numbers for quantity.');
	}
	//get price ------------------
	var priceText = quantityObj.previousSibling.previousSibling.previousSibling.nodeValue;
	var priceTextArray = priceText.split("$");
	var price = parseFloat(priceTextArray[priceTextArray.length-1]).toFixed(2);//parsefloat converts to Number, toFixed(2) forces it to have 2 decimal places
	//alert(price);
	createCookie(productName+'-P',price,1);
	updateCart();
}

function cancellOrder(){
	window.location.assign("default.aspx?id=4");
	emptyCellar();
}

function updateCart(){
	//get cookie values
	//alert('about to update cart');
	var cookieArray = document.cookie.split(';');
	//alert(cookieArray.length);
	var cartObj = xDOM('cartContents',0);
	if(cookieArray.length < 2){
		cartObj.innerHTML = "<center><span class='emptyCart'>Your Cellar is Empty</span></center>";
		return;
	}
	var prices = Array;
	var qty = Array;
	//alert('cookieArray.length ='+cookieArray.length);
	for(var i=0;i < cookieArray.length;i++) {
		//alert(i);
		var c = cookieArray[i];
		while (c.charAt(0)==' '){ c = c.substring(1,c.length);}//<--if the first charactor(s) is a space, remove it/them
		//cartObj.innerHTML = cartObj.innerHTML + c +'<br />';
		//alert(c);
		var cArr = c.split('=');
		var kindArr = cArr[0].split('-');
		//alert('hmmm');
		//alert('cArr[0]: '+cArr[0]);
		//alert('kindArr[0]: '+kindArr[0]);
		var productId = kindArr[0];
		//var kind = null;
		if (kindArr[kindArr.length-1] == 'P'){
			//it is a price cookie
			//alert('it is a price cookie');
			productId = productId+'-P';
			prices[productId] = parseFloat(cArr[cArr.length-1]).toFixed(2);//prices.productName = price value	
			//alert('prices[productId]: '+prices[productId]+'  || productId= '+productId);		
		}else if(kindArr[kindArr.length-1] == 'Q'){
			//it is a Quantity cookie
			//alert('it is a Quantity cookie');
			//alert(parseFloat(cArr[cArr.length-1]));
			productId = productId+'-Q';
			qty[productId] = parseFloat(cArr[cArr.length-1]);//quantity.productName = quantity value
			//alert('qty[productId]: '+qty[productId]+'  || productId= '+productId);
		}
	}
	//replace cart contents
	
	//alert('test');
	cartObj.innerHTML = '&nbsp;';
	var newInnerHTML = '<table  border="0" cellspacing="0" cellpadding="7">\
		  <tr>\
		    <td align="center" valign="middle" nowrap="nowrap" class="cartBold" >Remove</td>\
		    <td align="left" valign="middle" nowrap="nowrap" class="cartBold" >Product Name </td>\
		    <td align="center" valign="middle" nowrap="nowrap" class="cartBold" >Qty</td>\
		    <td align="right" valign="middle" nowrap="nowrap" class="cartBold" >Price</td>\
		  </tr>\
		  ';
	//alert('qty.length= '+qty.length);
	var runningTotal = 0;
	var bottlesTotal = 0;
	var evenOdd = 0;
	var theClass = null;
	for(var i in qty){
		//alert('qty[i]: '+qty[i]+' || i: '+i);
		var nameArr = i.split('-');
		var productName = nameArr[0];
		//var fixedName = productName.replace(/_/," "); //<-- replaces underscores in name with spaces
		var fixedName = productName.replaceAll("_", " ");
		if(nameArr[nameArr.length-1] == 'Q'){
		//var productName = i.id.replace(/_/," "); //<-- replaces underscores in name with spaces
		//alert(productName);
		var thePrice = prices[productName+'-P'];
		var formattedPrice = formatCurrency(thePrice);
		var theQuantity = qty[productName+'-Q'];
		if(thePrice > 0 && theQuantity >0){
			if(evenOdd == 0){ 
				theClass = 'odd';
				evenOdd = 1;
			}else if(evenOdd == 1){
				theClass = 'even';
				evenOdd = 0;
			}
		newInnerHTML = newInnerHTML + '<tr class="'+theClass+'" >\
		    <td align="center" valign="middle" nowrap="nowrap" ><img src="images/remove.gif" onclick="removeItem(\''+ productName +'\');" class="removeBtn" alt="remove" border="0" /></td>\
		    <td align="left" valign="middle" nowrap="nowrap" >'+ fixedName +'</td>\
		    <td align="center" valign="middle" nowrap="nowrap" >'+ theQuantity +'</td>\
		    <td align="right" valign="middle" nowrap="nowrap" >'+ formattedPrice +'</td>\
			</tr>';
		}
			runningTotal = runningTotal+(thePrice * theQuantity);
			bottlesTotal = bottlesTotal + theQuantity;
		}
		
	}
	if(theClass =='even'){
		theClass = 'odd'
	} else if(theClass == 'odd'){
		theClass = 'even';		
	}
	var Total = formatCurrency(runningTotal);
	newInnerHTML = newInnerHTML+'\
		  <tr class="'+theClass+'" >\
		    <td>&nbsp;</td>\
		    <td>&nbsp;</td>\
		    <td class="cartBold" >subTotal: </td>\
		    <td class="cartBold" align="right" valign="middle" nowrap="nowrap" >'+Total+'</td>\
		  </tr>\
		  <tr>\
		    <td colspan="4" align="right" valign="middle" nowrap="nowrap" ><div class="cellarBtn" id="emptyBtn" onclick="emptyCellar();" >Empty Cellar</div><a href="https://sturinotrotta.com/order.aspx" target="_top" ><div class="cellarBtn" id="orderBtn">Place Order</div></a></td>\
		  </tr>\
		</table>';
		if(runningTotal >0){
			cartObj.innerHTML = newInnerHTML;
		}else{
			cartObj.innerHTML = '<center><span class="emptyCart">Your Cellar is Empty</span></center>';
		}
		
	//alert('the cart has been updated');
}
var billingState = "";
var shippingState = "";
function summarizeOrder(bState, sState){
	
	if(bState === '-- select one --'){
		bState = null;
	}
	if(sState === '-- select one --'){
		sState = null;
	}
	
	
	//alert('bState = '+bState+' | sState = '+sState)
	var shippingFields = [
		$('txt_shippingName').value.clean(),
		$('txt_shipping_Street').value.clean(),
		$('txt_shippingCity').value.clean(),
		$('txt_shippingZip').value.clean(),
		$('txt_shippingPhone').value.clean(),
		$('txt_shippingEmail').value.clean()
	];
	
	//if any of the shipping fields are empty use billing state to calculate shipping
	if(shippingFields.contains('')){ sState = null; }	
	
	if(bState){ billingState = bState; 	}
	if(sState){ shippingState = sState; }
	forTextBox(sState,bState);
	forDisplay(sState,bState);
}

function forDisplay(shippingState, billingState){
	var cookieArray = document.cookie.split(';');
	var cartObj = xDOM('orderSummary',0);
	if(cookieArray.length < 2){
		cartObj.innerHTML = "<center><span class='emptyCart'>Your Cellar is Empty</span></center>";
		return;
	}
	var prices = Array;
	var qty = Array;
	//alert('cookieArray.length ='+cookieArray.length);
	for(var i=0;i < cookieArray.length;i++) {
		//alert(i);
		var c = cookieArray[i];
		while (c.charAt(0)==' '){ c = c.substring(1,c.length);}//<--if the first charactor(s) is a space, remove it/them
		//cartObj.innerHTML = cartObj.innerHTML + c +'<br />';
		//alert(c);
		var cArr = c.split('=');
		var kindArr = cArr[0].split('-');
		//alert('hmmm');
		//alert('cArr[0]: '+cArr[0]);
		//alert('kindArr[0]: '+kindArr[0]);
		var productId = kindArr[0];
		//var kind = null;
		if (kindArr[kindArr.length-1] == 'P'){
			//it is a price cookie
			//alert('it is a price cookie');
			productId = productId+'-P';
			prices[productId] = parseFloat(cArr[cArr.length-1]).toFixed(2);//prices.productName = price value	
			//alert('prices[productId]: '+prices[productId]+'  || productId= '+productId);		
		}else if(kindArr[kindArr.length-1] == 'Q'){
			//it is a Quantity cookie
			//alert('it is a Quantity cookie');
			//alert(parseFloat(cArr[cArr.length-1]));
			productId = productId+'-Q';
			qty[productId] = parseFloat(cArr[cArr.length-1]);//quantity.productName = quantity value
			//alert('qty[productId]: '+qty[productId]+'  || productId= '+productId);
		}
	}
	//replace cart contents
	
	//alert('test');
	cartObj.innerHTML = '&nbsp;';
	var newInnerHTML = '<table class="summaryTable"  border="0" cellspacing="0" cellpadding="7">\
		  <tr>\
		    <td align="left" valign="middle" nowrap="nowrap" class="titleRow" >Product Name </td>\
		    <td align="center" valign="middle" nowrap="nowrap" class="titleRow" >Qty</td>\
		    <td align="right" valign="middle" nowrap="nowrap" class="titleRow" >Price</td>\
		  </tr>\
		  ';
	//alert('qty.length= '+qty.length);
	var runningTotal = 0;
	var bottleTotal = 0;
	var evenOdd = 0;
	var theClass = null;
	for(var i in qty){
		//alert('qty[i]: '+qty[i]+' || i: '+i);
		var nameArr = i.split('-');
		var productName = nameArr[0];
		//var fixedName = productName.replace(/_/," "); //<-- replaces underscores in name with spaces
		var fixedName = productName.replaceAll("_", " ");
		if(nameArr[nameArr.length-1] == 'Q'){
		//var productName = i.id.replace(/_/," "); //<-- replaces underscores in name with spaces
		//alert(productName);
		var thePrice = prices[productName+'-P'];
		var formattedPrice = formatCurrency(thePrice);
		var theQuantity = qty[productName+'-Q'];
		if(thePrice > 0 && theQuantity >0){
			if(evenOdd == 0){ 
				theClass = 'odd';
				evenOdd = 1;
			}else if(evenOdd == 1){
				theClass = 'even';
				evenOdd = 0;
			}
		newInnerHTML = newInnerHTML + '<tr class="'+theClass+'" >\
		    <td align="left" valign="middle" nowrap="nowrap" >'+ fixedName +'</td>\
		    <td align="center" valign="middle" nowrap="nowrap" >'+ theQuantity +'</td>\
		    <td align="right" valign="middle" nowrap="nowrap" >'+ formattedPrice +'</td>\
			</tr>';
		}
			runningTotal = runningTotal+(thePrice * theQuantity);
			bottleTotal = bottleTotal + theQuantity;
		}
		
	}
	if(theClass =='even'){
		theClass = 'odd'
	} else if(theClass == 'odd'){
		theClass = 'even';		
	}
	
	var discount = 0;
	var discountPercent = "0%";
	if(bottleTotal >= 12){
		discount = (10 *runningTotal)/100;
		discountPercent = "10%";
	}else if(bottleTotal >5 && bottleTotal< 12){
		discount =  (5 *runningTotal)/100;
		discountPercent = "5%";
	}
	//var Total = formatCurrency(runningTotal - discount);
	
	newInnerHTML = newInnerHTML+'\
		  <tr class="'+theClass+'" >\
		    <td colspan="2" class="cartBold" >subtotal: </td>\
		    <td class="cartBold" align="right" valign="middle" nowrap="nowrap" > '+ formatCurrency(runningTotal) +'</td>\
		  </tr>';
		  
	if(theClass =='even'){
		theClass = 'odd'
	} else if(theClass == 'odd'){
		theClass = 'even';		
	}
	
	//discount = case discount
	var runningTotal = runningTotal - discount;
	
	//Calculate Member Discounts before tax
/*
	var silverMemberTotal = runningTotal -((runningTotal * memberDiscounts.silver)/100 );
	var goldMemberTotal = runningTotal -((runningTotal * memberDiscounts.gold)/100);
	var collectorMemberTotal = runningTotal -((runningTotal * memberDiscounts.collector)/100);
	var collectorMemberPlusTotal = runningTotal -((runningTotal * memberDiscounts.collectorPlus)/100);
*/
	
	newInnerHTML = newInnerHTML+'\
		  <tr class="'+theClass+'" >\
		    <td colspan="2" class="cartBold" >'+ discountPercent +' Case Discount: </td>\
		    <td class="cartBold" align="right" valign="middle" nowrap="nowrap" > - '+ formatCurrency(discount) +'</td>\
		  </tr>';
		  
	if(theClass =='even'){
		theClass = 'odd'
	} else if(theClass == 'odd'){
		theClass = 'even';		
	}
	
	var salesTax = 0;
	
	if(billingState == 'California'){
		salesTax = 7.25;
	}
	
	var taxAmount = (salesTax * runningTotal) / 100;
	runningTotal = runningTotal + taxAmount;
	
	//calculate taxes for member totals
/*
	var silverMemberTaxAmount = (salesTax * silverMemberTotal)/100;
	var goldMemberTaxAmount = (salesTax * goldMemberTotal)/100;
	var collectorMemberTaxAmount = (salesTax * collectorMemberTotal)/100;
	var collectorPlusMemberTaxAmount = (salesTax * collectorMemberPlusTotal)/100;
	silverMemberTotal += silverMemberTaxAmount;
	goldMemberTotal+= goldMemberTaxAmount;
	collectorMemberTotal+= collectorMemberTaxAmount;
	collectorMemberPlusTotal+= collectorPlusMemberTaxAmount;
*/
	
	
	
	
	
	//alert(billingState);
	newInnerHTML = newInnerHTML+'\
		  <tr class="'+theClass+'" >\
		    <td colspan="2" class="cartBold" > '+ salesTax +'% Sales Tax: </td>\
		    <td class="cartBold" align="right" valign="middle" nowrap="nowrap" > '+ formatCurrency(taxAmount) +'</td>\
		  </tr>';
		  
	if(theClass =='even'){
		theClass = 'odd'
	} else if(theClass == 'odd'){
		theClass = 'even';		
	}	
	
	//--- Add Shipping
	var shippingAmount = 0;
	var smallBoxPrice = 0;
	var largeBoxPrice = 0;
	var numOfLargeBoxes = 0;
	var numOfSmallBoxes = 0;
	//alert(shippingState);
	if(!shippingState || shippingState == false || shippingState == ""){ shippingState =  billingState;}
	
		if(shippingState == 'California'){
			largeBoxPrice = 19;
			smallBoxPrice = 14;
		}else if(
			shippingState == 'Colorado' || 
			shippingState == 'Idaho' || 
			shippingState == 'New Mexico' || 
			shippingState == 'Nevada' ||
			shippingState == 'Oregon' ||
			shippingState == 'Texas' ||
			shippingState == 'Washington' ||
			shippingState == 'Washington D.C.'
		){
			largeBoxPrice = 22;
			smallBoxPrice = 15;
		}else if(
			shippingState == 'Florida' || 
			shippingState == 'Georgia' || 
			shippingState == 'Iowa' || 
			shippingState == 'Louisiana' || 
			shippingState == 'Missouri' || 
			shippingState == 'Minnesota' || 
			shippingState == 'Ohio' || 
			shippingState == 'New York' || 
			shippingState == 'Virginia' || 
			shippingState == 'Wisconsin' || 
			shippingState == 'West Virginia' || 
			shippingState == 'Illinois'
		){
			largeBoxPrice = 35;
			smallBoxPrice = 22;
		}else if(
			shippingState == 'Alaska' ||
			shippingState == 'Hawaii'
		){
			largeBoxPrice = 80;
			smallBoxPrice = 50;
		}else{
			largeBoxPrice = 35;
			smallBoxPrice = 22;
		}
	
	if(bottleTotal > 6){
		numOfLargeBoxes = Math.round( bottleTotal/12);
		var remainder = bottleTotal - (numOfLargeBoxes * 12);
		if(remainder <7){
			numOfSmallBoxes = 1;
		}else{
			numOfLargeBoxes+=1;
		}	
	}else{
		numOfSmallBoxes = 1;
	}
	
	shippingAmount = (numOfLargeBoxes * largeBoxPrice) + (numOfSmallBoxes * smallBoxPrice);
	
	//alert('shippingState = ' +shippingState+ ' | numOfLargeBoxes = '+numOfLargeBoxes + ' | numOfSmallBoxes = '+numOfSmallBoxes + ' | shippingAmount = '+ shippingAmount);
	
	runningTotal = runningTotal + shippingAmount;
	
	//add shipping to Member Grand Totals
	/*
silverMemberTotal+= shippingAmount;
	goldMemberTotal+= shippingAmount;
	collectorMemberTotal+= shippingAmount;
	collectorMemberPlusTotal+= shippingAmount;
*/
	
	//Update Member Discounts Box
	/*
$('silverTd').innerHTML = formatCurrency(silverMemberTotal);
	$('goldTd').innerHTML = formatCurrency(goldMemberTotal);
	$('collectorTd').innerHTML = formatCurrency(collectorMemberTotal);
	$('collectorPlusTd').innerHTML = formatCurrency(collectorMemberPlusTotal);
*/
	
	newInnerHTML = newInnerHTML+'\
		  <tr class="'+theClass+'" >\
		    <td colspan="2" class="cartBold" > Shipping Amount: </td>\
		    <td class="cartBold" align="right" valign="middle" nowrap="nowrap" > '+ formatCurrency(shippingAmount) +'</td>\
		  </tr>';
		  
	if(theClass =='even'){
		theClass = 'odd'
	} else if(theClass == 'odd'){
		theClass = 'even';		
	}
	
	newInnerHTML = newInnerHTML+'\
		  <tr class="'+theClass+'" >\
		    <td colspan="2" class="cartBold" >Grand Total: </td>\
		    <td class="cartBold" align="right" valign="middle" nowrap="nowrap" >'+ formatCurrency(runningTotal) +'</td>\
		  </tr>\
		</table>';
		if(runningTotal >0){
			cartObj.innerHTML = newInnerHTML;
		}else{
			cartObj.innerHTML = '<center><span class="emptyCart">Your Cellar is Empty</span></center>';
		}
}

function forTextBox(shippingState, billingState){
	var summaryObj = xDOM('orderSummaryTextBox',0);
	
	var cookieArray = document.cookie.split(';');

	if(cookieArray.length < 2){
		//alert('empty cellar');
		summaryObj.innerHTML = "Your Cellar is Empty";
		return;
	}
	var prices = Array;
	var qty = Array;
	//alert('cookieArray.length ='+cookieArray.length);
	for(var i=0;i < cookieArray.length;i++) {
		//alert(i);
		var c = cookieArray[i];
		while (c.charAt(0)==' '){ c = c.substring(1,c.length);}//<--if the first charactor(s) is a space, remove it/them
		//cartObj.innerHTML = cartObj.innerHTML + c +'<br />';
		//alert(c);
		var cArr = c.split('=');
		var kindArr = cArr[0].split('-');
		//alert('hmmm');
		//alert('cArr[0]: '+cArr[0]);
		//alert('kindArr[0]: '+kindArr[0]);
		var productId = kindArr[0];
		//var kind = null;
		if (kindArr[kindArr.length-1] == 'P'){
			//it is a price cookie
			//alert('it is a price cookie');
			productId = productId+'-P';
			prices[productId] = parseFloat(cArr[cArr.length-1]).toFixed(2);//prices.productName = price value	
			//alert('prices[productId]: '+prices[productId]+'  || productId= '+productId);		
		}else if(kindArr[kindArr.length-1] == 'Q'){
			//it is a Quantity cookie
			//alert('it is a Quantity cookie');
			//alert(parseFloat(cArr[cArr.length-1]));
			productId = productId+'-Q';
			qty[productId] = parseFloat(cArr[cArr.length-1]);//quantity.productName = quantity value
			//alert('qty[productId]: '+qty[productId]+'  || productId= '+productId);
		}
	}
	//replace cart contents
	
	//alert('test');
	summaryObj.innerHTML = ' ';
	var newInnerHTML = ' ';
	//alert('qty.length= '+qty.length);
	var runningTotal = 0;
	var bottleTotal = 0;
	var evenOdd = 0;
	var theClass = null;
	for(var i in qty){
		//alert('qty[i]: '+qty[i]+' || i: '+i);
		var nameArr = i.split('-');
		var productName = nameArr[0];
		//var fixedName = productName.replace(/_/," "); //<-- replaces underscores in name with spaces
		var fixedName = productName.replaceAll("_", " ");
		if(nameArr[nameArr.length-1] == 'Q'){
		//var productName = i.id.replace(/_/," "); //<-- replaces underscores in name with spaces
		//alert(productName);
		var thePrice = prices[productName+'-P'];
		var formattedPrice = formatCurrency(thePrice);
		var theQuantity = qty[productName+'-Q'];
		if(thePrice > 0 && theQuantity >0){
			if(evenOdd == 0){ 
				theClass = 'odd';
				evenOdd = 1;
			}else if(evenOdd == 1){
				theClass = 'even';
				evenOdd = 0;
			}
		newInnerHTML = newInnerHTML +fixedName + ' '+theQuantity +' at '+ formattedPrice+'|';
		}
			runningTotal = runningTotal+(thePrice * theQuantity);
			bottleTotal = bottleTotal + theQuantity;
		}
		
	}
	if(theClass =='even'){
		theClass = 'odd'
	} else if(theClass == 'odd'){
		theClass = 'even';		
	}
	
	var discount = 0;
	var discountPercent = "0%";
	if(bottleTotal >= 12){
		discount = (10 *runningTotal)/100;
		discountPercent = "10%";
	}else if(bottleTotal >5 && bottleTotal< 12){
		discount =  (5 *runningTotal)/100;
		discountPercent = "5%";
	}
	//var Total = formatCurrency(runningTotal - discount);
	
	//--- Add Case Discount
	newInnerHTML = newInnerHTML +'subTotal: '+formatCurrency(runningTotal)+'|';
	newInnerHTML = newInnerHTML + discountPercent + ' Case Discount: ' + formatCurrency(discount)+'|';
	runningTotal = runningTotal - discount;
	
	//--- Calculate Member Discounts before tax
	/*
var silverMemberTotal = runningTotal -((runningTotal * memberDiscounts.silver)/100 );
	var goldMemberTotal = runningTotal -((runningTotal * memberDiscounts.gold)/100);
	var collectorMemberTotal = runningTotal -((runningTotal * memberDiscounts.collector)/100);
	var collectorMemberPlusTotal = runningTotal -((runningTotal * memberDiscounts.collectorPlus)/100);
*/
	
	//--- Add California Sales Tax
	var salesTax = 0;
	if(billingState == 'California'){ salesTax = 7.25; }
	var taxAmount = (salesTax * runningTotal) / 100;
	runningTotal = runningTotal + taxAmount;
	newInnerHTML = newInnerHTML + salesTax + '% Sales Tax: ' + formatCurrency(taxAmount) +'|';
	
	
	//--- Calculate taxes for Member Totals
/*
	var silverMemberTaxAmount = (salesTax * silverMemberTotal)/100;
	var goldMemberTaxAmount = (salesTax * goldMemberTotal)/100;
	var collectorMemberTaxAmount = (salesTax * collectorMemberTotal)/100;
	var collectorPlusMemberTaxAmount = (salesTax * collectorMemberPlusTotal)/100;
	silverMemberTotal += silverMemberTaxAmount;
	goldMemberTotal+= goldMemberTaxAmount;
	collectorMemberTotal+= collectorMemberTaxAmount;
	collectorMemberPlusTotal+= collectorPlusMemberTaxAmount;
*/
	
	
	//--- Add Shipping
	var shippingAmount = 0;
	var smallBoxPrice = 0;
	var largeBoxPrice = 0;
	var numOfLargeBoxes = 0;
	var numOfSmallBoxes = 0;
	//alert(shippingState);
	if(!shippingState || shippingState == false || shippingState == ""){ shippingState =  billingState;}
	
		if(shippingState == 'California'){
			largeBoxPrice = 19;
			smallBoxPrice = 14;
		}else if(
			shippingState == 'Colorado' || 
			shippingState == 'Idaho' || 
			shippingState == 'New Mexico' || 
			shippingState == 'Nevada' ||
			shippingState == 'Oregon' ||
			shippingState == 'Texas' ||
			shippingState == 'Washington' ||
			shippingState == 'Washington D.C.'
		){
			largeBoxPrice = 22;
			smallBoxPrice = 15;
		}else if(
			shippingState == 'Florida' || 
			shippingState == 'Georgia' || 
			shippingState == 'Iowa' || 
			shippingState == 'Louisiana' || 
			shippingState == 'Missouri' || 
			shippingState == 'Minnesota' || 
			shippingState == 'Ohio' || 
			shippingState == 'New York' || 
			shippingState == 'Virginia' || 
			shippingState == 'Wisconsin' || 
			shippingState == 'West Virginia' || 
			shippingState == 'Illinois'
		){
			largeBoxPrice = 35;
			smallBoxPrice = 22;
		}else if(
			shippingState == 'Alaska' ||
			shippingState == 'Hawaii'
		){
			largeBoxPrice = 80;
			smallBoxPrice = 50;
		}else{
			largeBoxPrice = 35;
			smallBoxPrice = 22;
		}
	
	if(bottleTotal > 6){
		numOfLargeBoxes = Math.round( bottleTotal/12);
		var remainder = bottleTotal - (numOfLargeBoxes * 12);
		if(remainder <7){
			numOfSmallBoxes = 1;
		}else{
			numOfLargeBoxes+=1;
		}	
	}else{
		numOfSmallBoxes = 1;
	}
	
	shippingAmount = (numOfLargeBoxes * largeBoxPrice) + (numOfSmallBoxes * smallBoxPrice);
	
	//alert('shippingState = ' +shippingState+ ' | numOfLargeBoxes = '+numOfLargeBoxes + ' | numOfSmallBoxes = '+numOfSmallBoxes + ' | shippingAmount = '+ shippingAmount);
	
	
	runningTotal = runningTotal + shippingAmount;
	newInnerHTML = newInnerHTML+'Shipping Amount: '+ formatCurrency(shippingAmount) +'|';
	
	//--- Add shipping to Member Grand Totals
/*
	silverMemberTotal+= shippingAmount;
	goldMemberTotal+= shippingAmount;
	collectorMemberTotal+= shippingAmount;
	collectorMemberPlusTotal+= shippingAmount;
*/
	
	//--- Update Text Box with Member Totals	
/*
	newInnerHTML+= ' Silver Member Grand Total: '+ formatCurrency(silverMemberTotal)+'|';
	newInnerHTML+= ' Gold Member Grand Total: '+ formatCurrency(goldMemberTotal)+'|';
	newInnerHTML+= ' Collector Member Grand Total: '+ formatCurrency(collectorMemberTotal)+'|';
	newInnerHTML+= ' Collector Plus Member Grand Total: '+ formatCurrency(collectorMemberPlusTotal)+'|';
	
*/
	newInnerHTML = newInnerHTML+' Total: '+ formatCurrency(runningTotal);

	if(runningTotal >0){
		summaryObj.innerHTML = newInnerHTML;
	}else{
		summaryObj.innerHTML = 'Your Cellar is Empty';
	}
		
	//alert('the cart has been updated');
}

function removeItem(productId){
	//alert(productId);
	var currentQuantity = readCookie(productId+'-Q');
	currentQuantity = parseFloat(currentQuantity);
	//alert(currentQuantity);
	var newQuantity = currentQuantity -1;
	//alert(newQuantity);
	createCookie(productId+'-Q',newQuantity,1);
	updateCart();
}

function emptyCellar(){
	var Cookies = document.cookie.split(";");
	for ( var Cnt=0; Cnt < Cookies.length; Cnt++ ) {
		var CurCookie = Cookies[Cnt].split("=");
		if ( CurCookie[0] ) {
			createCookie(CurCookie[0],0,0);
		};
	};
	updateCart();
	//alert('all cookies cleared');
}


String.prototype.replaceAll = function(oldSubString,newSubString){
	// Replaces all instances of the given substring.
	//http://www.bennadel.com/blog/142-Ask-Ben-Javascript-String-Replace-Method.htm
	var strText = this;
	var intIndexOfMatch = strText.indexOf( oldSubString );
	// Keep looping while an instance of the target string
	// still exists in the string.
	while (intIndexOfMatch != -1){
	// Relace out the current instance.
	strText = strText.replace( oldSubString, newSubString )
	// Get the index of any next matching substring.
	intIndexOfMatch = strText.indexOf( oldSubString );
	}
	// Return the updated string with ALL the target strings
	// replaced out with the new substring.
	return( strText );
}

function formatCurrency(num) {
	//http://javascript.internet.com/forms/currency-format.html
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + '$' + num + '.' + cents);
}

function IsNumeric(PossibleNumber)
{
	//http://codingtips.blogspot.com/2004/07/javascript-function-is-numeric.html
	var PNum = new String(PossibleNumber);
	var regex = /[^0-9]/;
	return !regex.test(PNum);
}

//~~~~~~~~~~[ cookie functions ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Cookie functions are from http://www.quirksmode.org/js/cookies.html
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

//~~~~~~~~~~[ object functions ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Cross Browser DOM
// copyright Stephen Chapman, 4th Jan 2005
// you may copy this code but please keep the copyright notice as well
//http://javascript.about.com/library/blxdom.htm
var aDOM = 0, ieDOM = 0, nsDOM = 0; var stdDOM = document.getElementById;
if (stdDOM) aDOM = 1; else {ieDOM = document.all; if (ieDOM) aDOM = 1; else {
var nsDOM = ((navigator.appName.indexOf('Netscape') != -1)
&& (parseInt(navigator.appVersion) ==4)); if (nsDOM) aDOM = 1;}}
function xDOM(objectId, wS) {
	if (stdDOM) return wS ? document.getElementById(objectId).style:
	document.getElementById(objectId);
	if (ieDOM) return wS ? document.all[objectId].style: document.all[objectId];
	if (nsDOM) return document.layers[objectId];
}                  
// Object Functions
// copyright Stephen Chapman, 4th Jan 2005
//  you may copy these functions but please keep the copyright notice as well
//http://javascript.about.com/library/blobj2.htm
/*
function objWidth(objectID) {
	var obj = xDOM(objectID,0); 
	if(obj.offsetWidth) return  obj.offsetWidth; if (obj.clip) return obj.clip.width; return 0;
}        
function objHeight(objectID) {
	var obj = xDOM(objectID,0); 
	if(obj.offsetHeight) return  obj.offsetHeight; if (obj.clip) return obj.clip.height; return 0;
}    
function objLeft(objectID) {
	var obj = xDOM(objectID,0);
	var objs = xDOM(objectID,1); 
	if(objs.left) return objs.left; if (objs.pixelLeft) return objs.pixelLeft; if (obj.offsetLeft) return obj.offsetLeft; return 0;
} 
function objTop(objectID) {
	var obj = xDOM(objectID,0);var objs = xDOM(objectID,1); if(objs.top) return objs.top; if (objs.pixelTop) return objs.pixelTop; if (obj.offsetTop) return obj.offsetTop; return 0;
} 
function objRight(objectID) {
	return objLeft(objectID)+objWidth(objectID);
} 
function objBottom(objectID) {
	return objTop(objectID)+objHeight(objectID);
} 
function objLayer(objectID) {
	var objs = xDOM(objectID,1); if(objs.zIndex) return objs.zIndex; return 0;
}
function objVisible(objectID) {
	var objs = xDOM(objectID,1); if(objs.visibility == 'hide' || objs.visibility == 'hidden') return 'hidden'; return 'visible';
}*/