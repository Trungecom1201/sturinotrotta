<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Adapter_ItemDataCapturers_Default {
	protected $itemMapping = array(
		35   => 'product_name',
		255 => 'sku',
		395 => 'price',
		435 => 'quantity',
		495 => 'tax',
		565 => 'subtotal'
	);
	
	protected function mapItem($item) {
		$output = array();
		foreach ($item as $attribute) {
			$feed = $attribute['feed'];
			if (!array_key_exists($feed, $this->itemMapping)) {
				continue;
			}
			
			$output[$this->itemMapping[$feed]] = $this->cleanUpText($attribute['text']);
		}
		
		return $output;
	}
	
	public function adapt($data) {
		$workingData = $data['lines'];
		$itemData = $this->mapItem($workingData[0]);
		
		$labels = array();
		$values = array();
		
		array_shift($workingData);
		$lastWasLabel = false;
		foreach ($workingData as $option) {
			foreach ($option as $optionAttribute) {
				switch ($optionAttribute['feed']) {
					case 35:
						if ($lastWasLabel) {
							$values[] = null;
						}
						$labels[] = $this->cleanUpText($optionAttribute['text']);
						$lastWasLabel = true;
						break;
					case 40:
						$lastWasLabel = false;
						$values[] = $this->cleanUpText($optionAttribute['text']);
						break;
				}
			}
		}
		if ($lastWasLabel) {
			$values[] = null;
		}
		
		$optionOutput = array();
		foreach ($labels as $k => $v) {
			$optionOutput[] = array('name' => $v, 'value' => $values[$k]);
		}
		
		$itemData['options'] = $optionOutput;
		return array($itemData);
	}
	
	private function cleanUpText($text) {
		if (!is_array($text)) { return $this->cleanUpTextLine($text); }
		return array_reduce($text, array($this, 'combineText'));
	}
	
	private function combineText($a, $b) {
		return (($a !== null)? $a . "\n" : null) . $this->cleanUpTextLine($b);
	}
	
	private  function cleanUpTextLine($text) {
		return trim(strip_tags($text));
	}
}