<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Invoice  {
	public function getPdf($invoices = array()) {
		$invoiceData = $this->adaptInvoiceData($invoices);
		
		$pdf = new Zend_Pdf();
		$pdf->pages = $this->renderInvoicePages($invoiceData);
		return $pdf;
	}
	
	private function preDataGetMagentoSetup() {
		$translate = Mage::getSingleton('core/translate');
		$translate->setTranslateInline(false);
	}
	
	private function postDataGetMagentoRestore() {
	        $translate = Mage::getSingleton('core/translate');
		$translate->setTranslateInline(true);
	}
	
	private function renderInvoicePages($invoices) {
		return BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Invoice::RenderInvoices($invoices);
	}
	
	private function adaptInvoiceData($invoices) {
		$this->preDataGetMagentoSetup();
		$invoiceData = BenGribaudo_CustomizablePDFInvoice_Model_Adapter_Invoice::AdaptInvoices($invoices);
		$this->postDataGetMagentoRestore();
		return $invoiceData;
	}
}