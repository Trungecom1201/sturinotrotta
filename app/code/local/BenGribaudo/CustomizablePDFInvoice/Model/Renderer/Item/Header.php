<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Header extends BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Base {
	protected $defaultTextStyle = 'ItemHeader';
	private $invoice;
	
	public static function RenderContent($invoice, $settings) {
		$renderer = new BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Header($invoice, $settings);
		return $renderer->Render();
	}

	public function __construct($invoice, $settings) {
		parent::__construct($settings);
		$this->invoice = $invoice;
		$this->data = $this->invoice['labels']['item_columns'];
	}

	public function Render() {
		return $this->content('BoxContent', $this->content('HorizontalContainer', $this->Mapper()))
				->setBorderColor($this->settings->getBorderColor())
				->setBackgroundColor($this->titleBoxBackgroundColor = $this->settings->getTitleBoxBackgroundColor())
				->setMargin($this->margin(0, 0, 0, $this->settings->getItemSpacing()))
				->setPadding($this->margin(10, 5, 10, 4));
	}
}