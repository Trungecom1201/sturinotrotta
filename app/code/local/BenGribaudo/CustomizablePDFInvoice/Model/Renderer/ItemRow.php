<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_ItemRow {
	private $settings;
	
	public function __construct($settings) {
		$this->settings = $settings;
	}
	
	public function Render($item) {
		if ($item['type'] == 'Mage_Bundle_Model_Sales_Order_Pdf_Items_Invoice') {
			$baseClassName = (array_key_exists('grouping', $item))? "Grouping" : "Grouped";
		} else {
			$baseClassName = "Default";
		}
		
		$fullClassName = "BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_" . $baseClassName;
		$renderer = new $fullClassName($item, $this->settings);
		return $renderer->Render();
	}
}