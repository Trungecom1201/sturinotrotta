<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_HorizontalContainer extends BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	public static function create($content) {
		return new BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_HorizontalContainer($content);
	}

	protected function getContentHeight() {
		$maxValue = null;
		foreach ($this->content as $item) {
			$height = $item->getHeight();
			if ($height > $maxValue) {
				$maxValue = $height;
			}
		}
		
		return $maxValue;
	}
	
	protected function RenderContent($page, $xStart, $xEnd, $y) {
		$childCountNotRequestingWidth = count($this->content) - $this->ChildrenSpecifyingWidth();
		$totalWidth = $xEnd - $xStart;
		$xOffsetAmount = ($childCountNotRequestingWidth > 0) ? (($xEnd - $xStart - $this->SpecifiedChildWidthSum($totalWidth)) / $childCountNotRequestingWidth) : null;
		$xOffsetStart = $xStart;
		$xOffsetEnd = $xStart;

		foreach ($this->content as $item) {
			if ($item->getWidth() == null) {
				$xOffsetEnd += $xOffsetAmount;
			} else {
				$xOffsetEnd += $this->CalculateRequestedItemWidth($totalWidth, $item->getWidth());
			}
			$item->Render($page, $xOffsetStart, $xOffsetEnd, $y);
			$xOffsetStart = $xOffsetEnd;
		}
	}
	
	private function CalculateRequestedItemWidth($totalWidth, $width) {
		$matches = array();
		if (preg_match('/^(?P<percent>\d+)%/', $width, $matches)) {
			return $totalWidth * $matches['percent'] / 100;
		} else {
			return $width;
		}
	}
	
	private function SpecifiedChildWidthSum($totalWidth) {
		$requestedX = 0;
		foreach ($this->content as $item) {
			$width = $this->CalculateRequestedItemWidth($totalWidth, $item->getWidth());
			$requestedX += $width;
		}
		return $requestedX;
	}
	
	private function ChildrenSpecifyingWidth() {
		$count = 0;
		foreach ($this->content as $item) {
			if ($item->getWidth() != null) {
				$count += 1;
			}
		}
		return $count;
	}
}