<?php 
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_SizedContent extends BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	protected function getContentHeight() {
		return 10;
	}
	
	protected function getContentWidth() {
		return 20;
	}
	
	protected function RenderContent($page, $xStart, $xEnd, $y) {
		$page->saveGS();
		$page->setFillColor(new Zend_Pdf_Color_GrayScale(.5));
		$page->drawRectangle($xStart, $y, $xEnd, $y + $this->getHeight());
		$page->restoreGS();
	}
}