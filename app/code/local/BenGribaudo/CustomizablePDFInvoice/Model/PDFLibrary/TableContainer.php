<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_TableContainer extends BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	private $widths;
	public function setWidths($widths) { $this->widths = $widths; }
	
	protected function getContentHeight() {}
	protected function RenderContent($page, $xStart, $xEnd, $y) {
		return $this->AssembleContent()->Render($page, $xStart, $xEnd, $y);
	}
	
	protected function AssembleContent() {
		$content = array();
		foreach ($this->content as $k => $v) {
			if ($this->widths && array_key_exists($k, $this->widths)) {
				$v = BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer::Create($v)->setWidth($this->widths[$k]);
			}
			$content[] = $v;
		}
		return BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_HorizontalContainer::Create($content);
	}
}
