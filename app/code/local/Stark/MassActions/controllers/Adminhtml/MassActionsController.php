<?php
 
class Stark_MassActions_Adminhtml_MassActionsController extends Mage_Adminhtml_Controller_Action
{
    public function massCompleteAction() {
        $orderIds = $this->getRequest()->getPost('order_ids', array());

        $countCompletedOrder = 0;
        $countNonCompletedOrder = 0;
 
        foreach ($orderIds as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ( !$order->canInvoice() && !$order->canShip() ) {
                $countNonCompletedOrder++;
            } else {
                $this->invoiceOrder( $order );
                $this->shipOrder( $order );

                $countCompletedOrder++;
            }
        }

        if ($countCompletedOrder) {
            $this->_getSession()->addSuccess($this->__( '%s order(s) have been completed.', $countCompletedOrder) );
        }
        if ($countNonCompletedOrder) {
            $this->_getSession()->addError($this->__( '%s order(s) have not been completed because they were cancelled, closed, or already complete.', $countNonCompletedOrder) );
        }
        
        $this->_redirect('adminhtml/sales_order/');
    }


    public function massCancelAction() {
        $orderIds = $this->getRequest()->getPost('order_ids', array());

        $countCancelOrder = 0;
        $countNonCancelOrder = 0;

        foreach ($orderIds as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            $state = $order->getState();
            
            // do a regular cancel
            if ( $order->canCancel() ) {
                $order->cancel()->save();
                $countCancelOrder++;

            // set an order to canceled that has been invoiced
            } elseif ( $state != "complete" && $state != "canceled" && $state != "closed" ) {
                $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true);
                $order->save();
                $countCancelOrder++;
            
            } else {
                $countNonCancelOrder++;
            }
        }

        if ($countNonCancelOrder) {
            if ($countCancelOrder) {
                $this->_getSession()->addError($this->__('%s order(s) cannot be canceled', $countNonCancelOrder));
            } else {
                $this->_getSession()->addError($this->__('The order(s) cannot be canceled'));
            }
        }
        if ($countCancelOrder) {
            $this->_getSession()->addSuccess($this->__('%s order(s) have been canceled.', $countCancelOrder));
        }

        $this->_redirect('adminhtml/sales_order/');
    }


    private function invoiceOrder( $order ) {
        if ( $order->canInvoice() ) {
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            if ( $invoice->getTotalQty() ) {
                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
                $invoice->register();
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
                $transactionSave->save(); 
            }           
        }
    }

    private function shipOrder( $order ) {
        if ( $order->canShip() ) {
            $itemQty =  $order->getItemsCollection()->count();
            $shipment = Mage::getModel( 'sales/service_order', $order )->prepareShipment( $itemQty );
            $shipment = new Mage_Sales_Model_Order_Shipment_Api();
            $orderIncrementId = $order->getIncrementId();
            $shipmentId = $shipment->create( $orderIncrementId );
        }
    }
}