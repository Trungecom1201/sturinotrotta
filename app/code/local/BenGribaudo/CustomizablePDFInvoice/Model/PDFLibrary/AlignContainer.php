<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer extends BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	const AlignLeft = 1, AlignCenter = 2, AlignRight = 3;
	private $align, $width, $height;
	public static function create($content) {
		return new BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer($content);
	}

	public function setAlign($align) {
		$this->align = $align;
		return $this;
	}
	
	protected function getContentHeight() {
		return ($this->height != null)? $this->height : $this->content->getHeight();
	}
	
	protected function getContentWidth() {
		return ($this->width != null)? $this->width : null;
	}
	
	public function setWidth($width) {
		/*if ($width != null and !$this->percentageMeasurement($width) and $width < $this->content->getWidth()) {
			throw new OversizedChildItemException("Width");
		}*/
		$this->width = $width;
		
		return $this;
	}
	
	public function setHeight($height) {
		if ($height != null and !$this->percentageMeasurement($height) and $height < $this->content->getHeight()) {
			throw new OversizedChildItemException("Height");
		}	
		$this->height = $height;
		return $this;
	}
	
	protected function RenderContent($page, $xStart, $xEnd, $y) {
		if ($this->content->getWidth() == null) {
			// since width == null or 0, xStart's value is also used for Render's xEnd argument
			$this->content->Render($page, $xStart, $xStart, $y);
			return;
		}
		
		switch ($this->align) {
			case self::AlignRight:
				$actualXStart = $xEnd - $this->content->getWidth();
				$actualXEnd = $xEnd;
				break;
			case self::AlignCenter:
				$actualXStart = $xStart + ($xEnd - $xStart) / 2 - $this->content->getWidth() / 2;
				$actualXEnd = $xStart + ($xEnd - $xStart) / 2 + $this->content->getWidth() / 2;
				break;
			case self::AlignLeft:
			default:
				$actualXStart = $xStart;
				$actualXEnd = $xStart + $this->content->getWidth();
				break;
		}
		$this->content->Render($page, $actualXStart, $actualXEnd, $y);
	}
	
	private function percentageMeasurement($value) {
		return (preg_match('/^(?P<percent>\d+)%/', $value) == 1);
	}
}
class OversizedChildItemException extends Exception {}