<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Helper_Admin_PageSizes
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'Letter', 'label'=>Mage::helper('adminhtml')->__('Letter (8.5 in. * 11 in.)')),
	    array('value' => 'Letter-Landscape', 'label'=>Mage::helper('adminhtml')->__('Letter Landscape (11 in. * 8.5 in.)')),
            array('value' => 'A4', 'label'=>Mage::helper('adminhtml')->__('A4 (210 mm * 297 mm)')),
	    array('value' => 'A4-Landscape', 'label'=>Mage::helper('adminhtml')->__('A4 Landscape (297 mm * 210 mm)')),
	    array('value' => 'Custom', 'label'=>Mage::helper('adminhtml')->__('Custom (specify dimensions below)'))
        );
    }

}