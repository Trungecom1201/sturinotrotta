<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Totals extends BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Base {
	private $invoice;
	
	public static function RenderContent($invoice, $settings) {
		$renderer = new BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Totals($invoice, $settings);
		return $renderer->Render();
	}

	public function __construct($invoice, $settings) {
		parent::__construct($settings);
		$this->invoice = $invoice;
		$this->styles = array(
			'TotalLabel' => array(
				'font_size' => $this->settings->getTotalFontSize()
			),
			'TotalValue' => array(
				'font_size' => $this->settings->getTotalFontSize()
			)
		);
	}
	
	public function Render() {
		$totalColumnWidth = $this->settings->getColumnSubtotalWidth();
		$valueAlign = $this->settings->getColumnSubtotalAlign();
		$content = array();
		
		foreach ($this->invoice['totals'] as $total) {
			$content[] = $this->content('HorizontalContainer', array(
				$this->content('AlignContainer', $this->text($total['label'], 'TotalsLabel'))
					->setAlign(BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer::AlignRight)
					->setMargin($this->margin(2, 0, 2, 0)),
				$this->content('AlignContainer', $this->text($total['amount'], 'TotalsValue'))
					->setAlign($valueAlign)
					->setWidth($totalColumnWidth)
					->setMargin($this->margin(2, 0, 2, 0))
			))->setMargin($this->margin(0, 0, 0, $this->settings->getItemSpacing()));
		}
		
		return $this->content('VerticalContainer', $content);
	}
}