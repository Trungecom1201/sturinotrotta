<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_TextContent extends BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	private $text, $font, $fontSize, $color;
	
	public static function create($content) {
		return new BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_TextContent($content);
	}
	
	public function __construct($content) { 
		parent::__construct($content);
		$this->font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
		$this->fontSize = 10;
	}
	
	public function setFont($font) {
		$this->font = $font;
		return $this;
	}
	
	public function setFontSize($size) {
		$this->fontSize = $size;
		return $this;
	}
	
	public function setColor($color) {
		$this->color = $color;
		return $this;
	}
	
	protected function getContentHeight() {
		return ((substr_count($this->content, "\n")) * $this->LineHeight()) + $this->FirstLineHeight();
	}
	
	protected function getContentWidth() {
		$lines = explode("\n", $this->content);
		$width = 0;
		
		$stringMeasurer = new Mage_Sales_Model_Order_Pdf_Invoice();
		
		foreach ($lines as $line) {
			$lineWidth = $stringMeasurer->widthForStringUsingFontSize($line, $this->font, $this->fontSize);
			if ($lineWidth > $width) { $width = $lineWidth; }
		}
		return ceil($width * 10) / 10;
	}
	
	protected function RenderContent($page, $xStart, $xEnd, $y) {
		$fontSize = ($this->fontSize == null)? 10 : $this->fontSize;
	
		$page->saveGS();
		$page->setFont($this->font, $fontSize);
		
		if ($this->color) {
			$page->setFillColor($this->color);
		}
		
		foreach (preg_split("/\n/", $this->content) as $lineNumber => $line) {
			$page->drawText($line, $xStart, $y - ($this->LineHeight() * ($lineNumber)) - $this->Ascent(), 'UTF-8');
		}
		
		$page->restoreGS();
	}
	private function FirstLineHeight() {
		return $this->Ascent() + $this->EmsToUnits(abs($this->font->getDescent()));
	}
	private function Ascent() {
		return $this->EmsToUnits(abs($this->font->getAscent()));
	}
	private function LineHeight() {
		return $this->EmsToUnits($this->font->getLineHeight());
	}
	private function EmsToUnits($ems) {
		return $ems / $this->font->getUnitsPerEm() * $this->fontSize;
	}
}
