/**
 * jjjMenu -beta
 * @version 1.1
 * @author Jason John Jaeger
 * last modified April 21st 2007
 * 
 * 
 * ---<[ changeLog ]>---
 * May 7th 2007:
 * 		Made it so that the main shadow div is inserted via javascipt
 * May 6th 2007: 
 * 		Created initMenu function to set submenu maxOpacity, fix button widths, and create submenu dropshadows
 * 		Made it so that the inner shadow divs are inserted via javascript
 * May 5th 2007: 
 * 		Added shadowOpacity option
 * 		Changed the way the drop shadows work. (changed the whole structure of it to use pure css and no tables)
 * 		Removed options.shadowDistanceX and options.shadowDistanceY
 * April 21st 2007: Added function to make all subMenu buttons in a given sub menu as wide as the widest one in Firefox in OSX
 */

var options = Array();
options.hideDelay = 250;//<--delay before subMenus disappear after you take mouse off of them (in milliseconds, 500 == half second)
options.vertOffset = 0;
options.initalSubMenuXoffset = 8;
options.initalSubMenuYoffset = 2;
options.subsequentSubMenuXoffset = 0;
options.subsequentSubMenuYoffset = 0;
options.maxOpacity = 100;
options.shadow = false;
options.shadowOpacity = 60;

//if(is_ie && is_major < 7){ options.shadow = false;	}

//~~~~No Need to Edit below this line :)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var timeOuts = Array();

function smShow(idNumber, mySMid){

//alert('idNumber = '+idNumber+' | mySubMenuId = '+mySMid);
	
	fixBtnWidths(idNumber);
	//setShadow(idNumber);
	
	var btnId = "b-" + idNumber;
	//var btnObj = getObject(btnId);
	var btnObj = xDOM(btnId);
	var btnClass = btnObj.className;
	

	if(btnClass == "mButtonParent" || btnClass == "mButtonParentOver"){
		smShowInitial(idNumber,btnId);
		setClass(btnId, "mButtonParentOver");
		cancell_hideAllSubMenus();
		hideTheChildren('ow-'+idNumber);
		
		hideAllSubMenus('ow-'+idNumber);
		
		//when there are transitions, they will be triggered here
	}else if(btnClass == "smButtonParent" || btnClass == "smButtonParentOver"){
		
		if(btnClass == "smButtonParent" && mySMid){hideTheChildren(mySMid);}
		smShowSubsequent(idNumber,btnId);
		//alert('smButtonParent');
		setClass(btnId, "smButtonParentOver");
		//when there are transitions, they will be triggered here
		
	}
	
	
}
var btnWidthsAreFixed = Array();
function fixBtnWidths(idNumber){
	//this function makes the widths of all buttons in a sub menu as wide as the widest one
	//so far as I know this is only needed for Firefox in OSX
	
	var soWrapperId = "ow-" + idNumber;
	if(!btnWidthsAreFixed[soWrapperId]  ){
		var soWrapper = fetchThing(soWrapperId);
		var soWrapperObj = soWrapper.obj;
		var widest = 0;
		var btnArray = getElementsByClassName(soWrapperObj,"a","smButton");
		for(var i=0; i< btnArray.length; i++){
			var width = findObjWidth(btnArray[i]);
			if(width > widest){widest = width;}
		}
		var parentBtnArray = getElementsByClassName(soWrapperObj,"a","smButtonParent");
		for(var i=0; i< parentBtnArray.length; i++){
			var width = findObjWidth(parentBtnArray[i]);
			if(width > widest){widest = width;}
		}
	
		var btnPaddingWidth = 0;
		var btnBorderWidth = 0;
		//if(parentBtnArray.length + btnArray.length >1){
			for(var i=0; i< btnArray.length; i++){	
				btnArray[i].style.width = widest+'px';
				var btn = fetchThing(btnArray[i].id);
				btnPaddingWidth = btn.paddingWidth;
				btnBorderWidth = btn.borderWidth;
				
			}
			if(parentBtnArray.length >0){
				for(var i=0; i< parentBtnArray.length; i++){ 
					parentBtnArray[i].style.width = widest+'px';	
					var pBtn = fetchThing(parentBtnArray[i].id);
					btnPaddingWidth = pBtn.paddingWidth;
					btnBorderWidth = pBtn.borderWidth;
				}
			}
			var sMenu = fetchThing('sm-'+idNumber);
			var newWidth = widest+btnPaddingWidth+sMenu.borderWidth+soWrapper.borderWidth;
			soWrapperObj.style.width = newWidth+'px';
		//}
		btnWidthsAreFixed[soWrapperId]= true;
		
	}
}

function initMenu(){
	
	setOpacity();
	
	var owArray = getElementsByClassName(document, "div", "soWrapper");
	for(var i=0; i<owArray.length; i++){ 
		var idArr = owArray[i].id.split("-");
		var idNumber = idArr[idArr.length-1];
		//alert(idNumber);
		fixBtnWidths(idNumber)
		setShadow(idNumber);
	}
	
	hideAllSubMenus();
fixPngs();
}

 function setOpacity(){
 	if(is_ie && is_major < 7 && options.shadow){ options.maxOpacity = options.maxOpacity-10;}
	var btnArray = getElementsByClassName(document, "a", "smButton");
	for(var i=0; i<btnArray.length; i++){ changeOpac(options.maxOpacity,btnArray[i].id); }
	var pBtnArray = getElementsByClassName(document, "a", "smButtonParent");
	for(var i=0; i<pBtnArray.length; i++){ changeOpac(options.maxOpacity,pBtnArray[i].id); }
 }
function aTest(style){
	/*var styleArr = style.split("-");
	var newString = "";
	for(x in styleArr){
		newString = newString + styleArr[x].replace(styleArr[x].charAt(0), styleArr[x].charAt(0).toUpperCase() );
	}
	alert(newString);*/
	
}
 function smbOver(smId){
 	hideTheChildren(smId);
	//liveLog('you just hovered over a not parent button');
 }
 
 function hideTheChildren(smId){
 	notOver(smId);
 	//var smObj = getObject(smId);	
	var smObj = xDOM(smId);
 	var parentBtnArray = getElementsByClassName(smObj, "a", "smButtonParent");
	for(var i=0; i<parentBtnArray.length; i++){
		var id = parentBtnArray[i].id;
		var idArray = id.split("-");
		var menuNumber = idArray[idArray.length-1];
		var menuId = "ow-" + menuNumber;
		//liveLog('hideTheChildred just ran on:'+menuId);
		notOver(menuId);
		//when there are transitions, they will be called here and the hideMenu function will be called at the end of them
		hideMenu(menuId);
		hideTheChildren(menuId);
	}
 }
 var shadowSized = Array();
 function setShadow(idNumber){
 	if(options.shadow && !shadowSized[idNumber]){
		
		
		
		//create shadow div
		var sMenuIW_Obj = xDOM("iw-"+idNumber);
		var shadowDiv = document.createElement('div');
		shadowDiv.setAttribute('id', "shadow-" + idNumber);
		shadowDiv.setAttribute('class', "shadow");
		sMenuIW_Obj.appendChild(shadowDiv);//<--adds shadow inside innerWrappper AFTER sMenu div
		//----------------
		
		var sMenu = fetchThing("sm-" + idNumber);
		var shadow = fetchThing("shadow-" + idNumber);
		
		
		shadow.obj.style.width = sMenu.width+'px';
		shadow.obj.style.height = sMenu.height+'px';
		
		
		if(is_ie && is_major < 7){
			
			if(options.shadowOpacity){
				var ieOpacity = (options.shadowOpacity -25)/100;
				shadow.obj.style.backgroundColor = "white";
				shadow.obj.style.filter= "progid:DXImageTransform.Microsoft.blur(pixelradius=2.2, makeshadow='true', ShadowOpacity="+ ieOpacity +")";
				shadow.obj.style.position = "absolute";
				shadow.obj.style.top="0px";
			}

		}else{
			
					
			shadow.obj.innerHTML =  '<div class="shadow_trc" id="trc-'+ idNumber +'" ></div>'+
									'<div class="shadow_rm" id="rm-'+ idNumber +'" ></div>'+
									'<div class="shadow_brc" id="brc-'+ idNumber +'" ></div>'+
									'<div class="shadow_bm" id="bm-'+ idNumber +'" ></div>'+
									'<div class="shadow_blc" id="blc-'+ idNumber +'" ></div>';
								
			var trc = fetchThing("trc-" + idNumber);
			var rm = fetchThing("rm-" + idNumber);
			var brc = fetchThing("brc-" + idNumber);
			var bm = fetchThing("bm-" + idNumber);
			var blc = fetchThing("blc-" + idNumber);
			
			var newHeight = sMenu.height - trc.height;
			
			rm.obj.style.height = newHeight +'px';
			rm.obj.style.top = trc.height+'px'; 
			bm.obj.style.width = sMenu.width - blc.width - brc.width +'px';
			bm.obj.style.left = blc.width + brc.width +'px';
			
			
			if(options.shadowOpacity){ 	changeOpac(options.shadowOpacity,shadow.id); }
			
		}
		//shadow.obj.style.border = "1px dotted orange";
		
	}
 }
 
 function smShowInitial(idNumber, btnId){
 	//alert('btnId = '+btnId+' | idNumber = '+idNumber);
	//var btnObj = getObject(btnId);
	
	//fixBtnWidths(idNumber);
	
	
	var btnObj = xDOM(btnId);
	var btnLeft = findPosX(btnObj);
	var btnTop = findPosY(btnObj);
	var btnHeight = findObjHeight(btnObj);
	var soWrapperId = "ow-" + idNumber;
	// soWrapperObj = getObject(soWrapperId);
	var soWrapperObj = xDOM(soWrapperId);
	var vertOffset = 0;
	if(options.vertOffset && isNaN(options.vertOffset) == false){ vertOffset = options.vertOffset;	}
	if(is_ie){
		//vertOffset = vertOffset +35;
		//vertOffset = 0;
	}
	
	
	
	var newTop = btnTop + btnHeight + vertOffset + options.initalSubMenuYoffset;
	
	if(is_ie && is_major < 7){
		newTop = newTop -2;
	}
	
	if(is_gecko){
		//options.initalSubMenuXoffset = -98;
	}
	
	soWrapperObj.style.left = btnLeft+ options.initalSubMenuXoffset +'px';
	soWrapperObj.style.top = newTop+'px';
	soWrapperObj.style.visibility = "visible";
	
	
	//alert('newTop= '+newTop+' | findPosY= '+findPosY(soWrapperObj)+' | findPosY(btnObj)= '+findPosY(btnObj));
 }
 
 var sizeSet = Array();
 function smShowSubsequent(idNumber, btnId){
	
	//fixBtnWidths(idNumber);
	//alert('idNumber= '+idNumber+' | btnId= '+btnId);
 	var btn = fetchThing(btnId);
	var sow = fetchThing(("ow-" + idNumber));
	if(!sizeSet[sow.id]){
		// this is done so the sub menus don't get squished since they are elasticy via css to expand for inner content
		sow.obj.style.width = sow.width+'px';
		sizeSet[sow.id] = true;
	}
	//if(is_gecko){options.subsequentSubMenuXoffset = 1;}
	var newLeft = btn.right+options.subsequentSubMenuXoffset;
	if((btn.right + sow.width) > posRight()){
		//if the subsequent submenu would go off the right edge, appear on the other side of the parent submenu		
		
		newLeft = btn.left - sow.width + sow.paddingRight;
	}
	var newTop = btn.top;
	if(btn.top + sow.height > posBottom()){
		//if the subsequent submenu would go off the bottom edge, appear above the parent submenu btn
		newTop = btn.bottom - sow.height + sow.paddingBottom + sow.borderHeight + options.subsequentSubMenuYoffset;
	}
	
	
	//alert(newLeft);
	
	
	
	sow.obj.style.left = newLeft+'px';
	sow.obj.style.top = newTop+'px';
	sow.obj.style.visibility = "visible";
	
	if(options.maxOpacity){
		if(is_ie && is_major < 7){
			
		}else{
			//changeOpac(options.maxOpacity,sow.id);
		}
	}
	
 }
 
 function smHide(idNumber){
 	call_hideAllSubMenus();
 }
 
 function overSOwrapper(){
 	cancell_hideAllSubMenus();
	
 }
 
 function outSOwrapper(){
	call_hideAllSubMenus();
 }
 
function call_hideAllSubMenus(){
	cancell_hideAllSubMenus();
	timeOuts.hideAllSubMenus = setTimeout('hideAllSubMenus()',options.hideDelay);
	//liveLog('<b>hideAllSubMenus <span style="color:green;">called</span></b>');
}//end function

function cancell_hideAllSubMenus(){
	if(timeOuts.hideAllSubMenus){
		clearTimeout(timeOuts.hideAllSubMenus);
		//liveLog('<b>hideAllSubMenus <span style="color:red;">cancelled</span></b>');
	}//end if
}//end function

function hideAllSubMenus(exceptThisId){
	var smArray = getElementsByClassName(document, "div", "soWrapper");
	for(var i=0; i<smArray.length; i++){
		notOver(smArray[i].id, exceptThisId);
		if(exceptThisId != smArray[i].id){
			hideMenu(smArray[i].id);	
		}
		
		//when there are transitions, they will be called here and the hideMenu function will be called at the end of them
	}//end for
	
	//hello self, now create an exceptThisId for notOver
	
	notOver('mw', exceptThisId);//<-- main menu
	clearTimeout(timeOuts.hideAllSubMenus);
}//end function

function hideMenu(menuId){
	//var menuObj = getObject(menuId);
	var menuObj = xDOM(menuId);
	//liveLog('hideMenu just ran on:'+menuObj+" || "+menuId);
	menuObj.style.visibility = "hidden";
	var smHeight = findObjHeight(menuObj);
	menuObj.style.top = "-" + smHeight + "px";
	//menuObj.style.left = 0+'px';
}

function notOver(menuId, exceptThisMenuId){
	var exceptThisId = null;
	if(exceptThisMenuId){ 
		var idArray = exceptThisMenuId.split("-");
		var menuNumber = idArray[idArray.length-1];
		exceptThisId = 'b-'+menuNumber;
	}
	//var menuObj = getObject(menuId);
	var menuObj = xDOM(menuId);
	var mButtonParentOverArray = getElementsByClassName(menuObj, "a", "mButtonParentOver");
	for(var i=0; i<mButtonParentOverArray.length; i++){
		//alert(mButtonParentOverArray[i].id+' | '+exceptThisId);
		if(exceptThisId != mButtonParentOverArray[i].id ){
			setClass(mButtonParentOverArray[i].id, 'mButtonParent');
		}
	}//end for
	var smButtonParentOverArray = getElementsByClassName(menuObj, "a", "smButtonParentOver");
	for(var i=0; i<smButtonParentOverArray.length; i++){
		if(exceptThisId != smButtonParentOverArray[i].id){
			setClass(smButtonParentOverArray[i].id, 'smButtonParent');
		}
	}//end for
}
 
var debug = true;
function liveLog(message){
	if(debug){
		//var liveLogObj = getObject('liveLog');
		var liveLogObj = xDOM('liveLog');
		var currentContent = liveLogObj.innerHTML;
		liveLogObj.innerHTML = message.toString()+" --> "+Date().toString()+'<br />'+currentContent;
	}
}//end function
 
 
 
 //--[ common ]--------------------------------------------------------------------------------------------------------------------------
 function fetchThing(id){
 	var arr = Array();
	arr.id = id;
	arr.obj = xDOM(id);
	arr.cssClass = arr.obj.className;
	
	arr.width = findObjWidth(arr.obj);
	arr.height = findObjHeight(arr.obj);
	
	arr.left = findPosX(arr.obj);
	arr.right = arr.left + arr.width;
	arr.top = findPosY(arr.obj);
	arr.bottom = arr.top + arr.height;
	
	arr.paddingRight = fetchStyle(id, 'padding-right', true);
	arr.paddingLeft = fetchStyle(id,'padding-left',true);
	arr.paddingBottom = fetchStyle(id,'padding-bottom', true);
	arr.paddingTop = fetchStyle(id,'padding-top',true);
	arr.paddingHeight = arr.paddingBottom + arr.paddingTop;
	arr.paddingWidth = arr.paddingLeft + arr.paddingRight;
	
	arr.marginTop = fetchStyle(id,'margin-top',true);
	arr.marginBottom = fetchStyle(id,'margin-bottom',true);
	arr.marginLeft = fetchStyle(id,'margin-left', true);
	arr.marginRight = fetchStyle(id,'margin-right',true);
	arr.marginWidth = arr.marginLeft + arr.marginRight;
	arr.marginHeight = arr.marginBottom + arr.marginTop;	
	
	arr.borderBottomWidth = fetchStyle(id, 'border-bottom-width', true);
	arr.borderTopWidth = fetchStyle(id,'border-top-width', true);
	arr.borderLeftWidth = fetchStyle(id,'border-left-width',true);
	arr.borderRightWidth = fetchStyle(id, 'border-right-width', true);
	arr.borderHeight = arr.borderBottomWidth + arr.borderTopWidth;
	arr.borderWidth = arr.borderLeftWidth + arr.borderRightWidth;
	
	/*for(x in arr){
		liveLog('arr.' + x + ' = ' + arr[x]);
	}*/
	return arr;
 }
 
function fetchStyle(id, style, convertToNumber){
	//enter styles all lowercase with hyphen between words. ie: "border-right-width"
	// see http://codepunk.hardwar.org.uk/css2js.htm for css to javascript reference conversion
	var value = null;
	if(getStyle(id,style)){ value = getStyle(id, style); }
	//if that does not work, we take out all hyphens and capitalize first letter of each word and try again
	var styleArr = style.split("-");
	var newStyle = "";
	for(var i=0; i<styleArr.length; i++){
		if(i === 0){//don't capitolize the first letter!
			newStyle += styleArr[i];
		}else{
			newStyle += (styleArr[i].replace(styleArr[i].charAt(0),styleArr[i].charAt(0).toUpperCase()));
		}
	}
	if(getStyle(id,newStyle)){ 	value = getStyle(id, newStyle); }
	if(convertToNumber){value = parseFloat(value);}
	if(convertToNumber && isNaN(value)){ value = 0;}
	return value;
} 

/*
function getPaddingRight(id){
	//requires getStyle function
	var paddingRight = 0;
	if(getStyle(id,'padding-right')){
		paddingRight = getStyle(id,'padding-right');
	}else if(getStyle(id,'paddingRight')){
		paddingRight = getStyle(id,'paddingRight');
	}
	return parseFloat(paddingRight);
}

function getPaddingBottom(id){
	//requires getStyle function
	var paddingBottom = 0;
	if(getStyle(id,'padding-bottom')){
		paddingBottom = getStyle(id,'padding-bottom');
	}else if(getStyle(id,'paddingBottom')){
		paddingBottom = getStyle(id,'paddingBottom');
	}
	return parseFloat(paddingBottom);
}
*/
function getStyle(id,styleProp){
	//http://www.quirksmode.org/dom/getstyles.html
	//liveLog(styleProp);
	var y = null;
	var x = xDOM(id);
	if (x.currentStyle){
		y = x.currentStyle[styleProp];
	}else if (window.getComputedStyle){
		y = document.defaultView.getComputedStyle(x,null).getPropertyValue(styleProp);
	}
	return y;
}

function findObjHeight(thing){
	if(thing.offsetHeight){
		thing_height = thing.offsetHeight;
	}
	else if(thing.style.pixelHeight){
		thing_height = thing.pixelHeight;
	} 
	return thing_height;
}//end function
	
function findObjWidth(thing){
	//last modified March 04, 2007
	var thing_width = 0;
	if(thing.offsetWidth){
		thing_width = thing.offsetWidth;
	}
	else if(thing.style.pixelWidth){
		thing_width = thing.pixelWidth;
	} 
	return thing_width;
}//end function
	
function findPosY(obj)  {
	// by Peter-Paul Koch & Alex Tingle
	// http://blog.firetree.net/2005/07/04/javascript-find-position/
	// http://www.quirksmode.org/js/findpos.html
	var curtop = 0;
    
	if(obj.offsetParent){
        while(1)
        {
		//alert('curtop= '+curtop);
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
		  {
            break;
		  }
          obj = obj.offsetParent;
        }
   } else if(obj.y){
        
		curtop += obj.y;
	}
    return curtop;
  }//end function
  
function findPosX(obj){
	// by Peter-Paul Koch & Alex Tingle
	// http://blog.firetree.net/2005/07/04/javascript-find-position/
	// http://www.quirksmode.org/js/findpos.html
    var curleft = 0;
    if(obj.offsetParent){
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
		  {
            break;
		  }
          obj = obj.offsetParent;
        }
   } else if(obj.x)
   	{
    	curleft += obj.x;
	}
    return curleft;
  }//end function
  
function getElementsByClassName(oElm, strTagName, strClassName){
	/* this awesome getElementsByClassName function was written by:
	Jonathan Snook, http://www.snook.ca/jonathan
	with add-ons by:
	Robert Nyman, http://www.robertnyman.com
	and be found at http://www.robertnyman.com/2005/11/07/the-ultimate-getelementsbyclassname/
	*/
    var arrElements = (strTagName == "*" && document.all)? document.all : oElm.getElementsByTagName(strTagName);
    var arrReturnElements = Array();
    strClassName = strClassName.replace(/\-/g, "\\-");
    var oRegExp = new RegExp("(^|\\s)" + strClassName + "(\\s|$)");
    var oElement;
    for(var i=0; i<arrElements.length; i++){
        oElement = arrElements[i];      
        if(oRegExp.test(oElement.className)){arrReturnElements.push(oElement);}//end if 
    }//end for
    return (arrReturnElements);
}//end function

function setClass(id, className){
	// I made this function from David F. Miller's code (at A List apart) which can be found at:
	// http://www.alistapart.com/articles/jslogging
	// if the node's class already exists then replace its value
	//var obj = getObject(id);
	var obj = xDOM(id);
	if (obj.getAttributeNode("class")) {
	  for (var i = 0; i < obj.attributes.length; i++) {
		var attrName = obj.attributes[i].name.toUpperCase();
		if (attrName == 'CLASS') {
		  obj.attributes[i].value = className;
		}//end if
	  }//end for
	// otherwise create a new attribute
	} else {
	  obj.setAttribute("class", className);
	}//end if else
}//end function

//###V--[ Browser Window Size and Scroll Position functions ]##################################
// Browser Window Size and Position
// copyright Stephen Chapman, 3rd Jan 2005, 8th Dec 2005
// you may copy these functions but please keep the copyright notice as well
function pageWidth() {return window.innerWidth != null? window.innerWidth : document.documentElement && document.documentElement.clientWidth ?       document.documentElement.clientWidth : document.body != null ? document.body.clientWidth : null;} 
function pageHeight() {return  window.innerHeight != null? window.innerHeight : document.documentElement && document.documentElement.clientHeight ?  document.documentElement.clientHeight : document.body != null? document.body.clientHeight : null;} 
function posLeft() {return typeof window.pageXOffset != 'undefined' ? window.pageXOffset :document.documentElement && document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ? document.body.scrollLeft : 0;} 
function posTop() {return typeof window.pageYOffset != 'undefined' ?  window.pageYOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ? document.body.scrollTop : 0;} 
function posRight() {return posLeft()+pageWidth();} 
function posBottom() {return posTop()+pageHeight();}
                    

//~~~~~~~~~~[ xDOM ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Cross Browser DOM
// copyright Stephen Chapman, 4th Jan 2005
// you may copy this code but please keep the copyright notice as well
//http://javascript.about.com/library/blxdom.htm
var aDOM = 0, ieDOM = 0, nsDOM = 0; var stdDOM = document.getElementById;
if (stdDOM) aDOM = 1; else {ieDOM = document.all; if (ieDOM) aDOM = 1; else {
var nsDOM = ((navigator.appName.indexOf('Netscape') != -1)
&& (parseInt(navigator.appVersion) ==4)); if (nsDOM) aDOM = 1;}}
function xDOM(objectId, wS) {
	if (stdDOM) return wS ? document.getElementById(objectId).style:
	document.getElementById(objectId);
	if (ieDOM) return wS ? document.all[objectId].style: document.all[objectId];
	if (nsDOM) return document.layers[objectId];
}                  
// Object Functions
// copyright Stephen Chapman, 4th Jan 2005
//  you may copy these functions but please keep the copyright notice as well
//http://javascript.about.com/library/blobj2.htm
function objWidth(objectID) {
	var obj = xDOM(objectID,0); 
	if(obj.offsetWidth) return  parseFloat(obj.offsetWidth); if (obj.clip) return parseFloat(obj.clip.width); return 0;
}        
function objHeight(objectID) {
	var obj = xDOM(objectID,0); 
	if(obj.offsetHeight) return  parseFloat(obj.offsetHeight); if (obj.clip) return parseFloat(obj.clip.height); return 0;
}    
function objLeft(objectID) {
	var obj = xDOM(objectID,0);
	var objs = xDOM(objectID,1); 
	if(objs.left) return parseFloat(objs.left); if (objs.pixelLeft) return parseFloat(objs.pixelLeft); if (obj.offsetLeft) return parseFloat(obj.offsetLeft); return 0;
} 
function objTop(objectID) {
	var obj = xDOM(objectID,0);var objs = xDOM(objectID,1); if(objs.top) return parseFloat(objs.top); if (objs.pixelTop) return parseFloat(objs.pixelTop); if (obj.offsetTop) return parseFloat(obj.offsetTop); return 0;
} 
function objRight(objectID) {
	return parseFloat(objLeft(objectID))+parseFloat(objWidth(objectID));
} 
function objBottom(objectID) {
	return parseFloat(objTop(objectID))+parseFloat(objHeight(objectID));
} 
function objLayer(objectID) {
	var objs = xDOM(objectID,1); if(objs.zIndex) return objs.zIndex; return 0;
}
function objVisible(objectID) {
	var objs = xDOM(objectID,1); if(objs.visibility == 'hide' || objs.visibility == 'hidden') return 'hidden'; return 'visible';
}

function changeOpac(opacity, id) {//##########################################################
	// I did not write this function it can be found at:
	// http://brainerror.net/scripts_js_blendtrans.php
	var object = document.getElementById(id).style; 
	object.opacity = (opacity / 100);
	object.MozOpacity = (opacity / 100);
	object.KhtmlOpacity = (opacity / 100);
	object.filter = "alpha(opacity=" + opacity + ")";
}//end function
                    