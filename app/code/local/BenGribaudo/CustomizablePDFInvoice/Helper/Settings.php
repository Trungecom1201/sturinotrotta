<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Helper_Settings {
	public function getPageWidth() {
		switch ($this->setting('page_size')) {
			case 'Letter':
				$width = 612;
				break;
			case 'Letter-Landscape':
				$width = 792;
				break;
			case 'A4':
				$width = 595;
				break;
			case 'A4-Landscape':
				$width = 842;
				break;
			case 'Custom':
				$width = $this->setting('page_width');
				break;
		}
		
		return $width;
	}
	public function getPageHeight() {
		switch ($this->setting('page_size')) {
			case 'Letter':
				$height = 792;
				break;
			case 'Letter-Landscape':
				$height = 612;
				break;
			case 'A4':
				$height = 842;
				break;
			case 'A4-Landscape':
				$height = 595;
				break;
			case 'Custom':
				$height = $this->setting('page_height');
				break;
		}
		
		return $height;
	}
	
	public function __call($name, $arguments) {
		if (!preg_match('/^get/', $name)) {
			throw new Exception("Attepted to call undefined method - " . $name);
		}
		
		$name = preg_replace('/^get/', '', $name);
		$name = strtolower(preg_replace('/(?<=.)([A-Z])/', '_$1', $name));

		if (preg_match('/_font$/', $name)) {
			return $this->fontByName($this->setting($name));
		} elseif (preg_match('/totals_[^_]+_font_size/', $name)) {
			return $this->setting('totals_font_size');
		} elseif (preg_match('/_align$/', $name)) {
			return $this->alignmentByName($this->setting($name));
		} elseif (preg_match('/_width$/', $name)) {
			return $this->width($this->setting($name));
		}
		return $this->setting($name);
	}
	public function getBorderColor() {
		return new Zend_Pdf_Color_GrayScale(0.5);
	}
	public function getTitleBoxBackgroundColor() {
		return new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92);
	}
	private function setting($setting) {
		return Mage::getStoreConfig('sales_pdf/bengribaudo_customizableinvoice/' . $setting);
	}
	private function width($value) {
		return ($value >= 0)? $value : null;
	}
	private function fontByName($name) {
		switch ($name) {
			case "Libertine":
				return Zend_Pdf_Font::fontWithPath(Mage::getBaseDir() . '/lib/LinLibertineFont/LinLibertineC_Re-2.8.0.ttf');
			case "Libertine-Bold":
				return Zend_Pdf_Font::fontWithPath(Mage::getBaseDir() . '/lib/LinLibertineFont/LinLibertine_Bd-2.8.1.ttf');
			case "Libertine-Italic":
				return Zend_Pdf_Font::fontWithPath(Mage::getBaseDir() . '/lib/LinLibertineFont/LinLibertine_It-2.8.2.ttf');
			default:
				return Zend_Pdf_Font::fontWithName($name);
		}
	}	
	private function alignmentByName($name) {
		switch($name) {
			case "Center":
				return BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer::AlignCenter;
			case "Right":
				return BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer::AlignRight;
			case "Left":
			default:
				return BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer::AlignLeft;
		}
	}
}