<?php
class Stark_MassActions_Model_Observer
{
    public function addMassAction($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'sales_order')
        {
            $block->addItem('starkmassactions_complete', array(
                'label' => 'Complete',
                'url' => Mage::app()->getStore()->getUrl('starkmassactions/adminhtml_massActions/massComplete')
            ));
            $block->addItem('starkmassactions_cancel', array(
                'label' => 'Cancel',
                'url' => Mage::app()->getStore()->getUrl('starkmassactions/adminhtml_massActions/massCancel')
            ));
        }
    }
}