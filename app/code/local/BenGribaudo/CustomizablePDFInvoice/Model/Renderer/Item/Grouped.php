<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Grouped extends BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Default {
	protected $productNameStyle = 'ItemGrouped';
	public function __construct($item, $settings) {
		parent::__construct($item, $settings);
		$this->styles = array(
			'ItemGrouped' => array(
				'font' => $this->settings->getItemFont(),
				'font_size' => $this->settings->getItemFontSize(),
				'margin' => new BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Margin($this->settings->getItemGroupedIndentation(), 0, 0, 0)
			)
		);
	}

}