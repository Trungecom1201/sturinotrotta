<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Default extends BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Base {
	protected $item;
	protected $productNameKey = 'product_name', $productNameStyle = 'Item';
	
	public function __construct($item, $settings) {
		parent::__construct($settings);
		$this->item = $item;
		$this->data = $this->item;
	}
	
	public function Render() {
		$this->data['product_name'] = $this->productNameContent();
		$item = $this->content("HorizontalContainer", $this->Mapper());

		if (array_key_exists('options', $this->item)) {
			$content = array($item);
			
			foreach ($this->item['options'] as $option) {
				$value = (array_key_exists('value', $option))? $option['value'] : null;
				$attributeText = $option['name'] . ': ' . $value;
				$content[] = $this->CreateColumn($attributeText, null, BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer::AlignLeft)
							->setMargin($this->margin(10, $this->settings->getItemSpacing(), 0, 0));
			}
			
			$output = $this->content("VerticalContainer", $content);
		} else {
			$output = $item;
		}
		
		return $output->setMargin($this->margin(0, 0, 0, $this->settings->getItemSpacing()));
	}
		
	private function productNameContent() {
		return $this->text($this->item[$this->productNameKey], $this->productNameStyle);
	}
}