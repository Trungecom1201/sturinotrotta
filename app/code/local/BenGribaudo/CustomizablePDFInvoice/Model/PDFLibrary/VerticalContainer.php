<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_VerticalContainer extends BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	public static function create($content) {
		return new BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_VerticalContainer($content);
	}

	protected function getContentHeight() {
		$totalHeight = 0;
		foreach ($this->content as $item) {
			$totalHeight += $item->getHeight();
		}
		
		return $totalHeight;
	}
	protected function RenderContent($page, $xStart, $xEnd, $y) {
		$startingY = $y;
		foreach ($this->content as $item) {
			$item->Render($page, $xStart, $xEnd, $startingY);
			$startingY -= $item->getHeight();
		}
	}
	
	
}