<?php
/**
* BSS Commerce Co.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://bsscommerce.com/Bss-Commerce-License.txt
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento COMMUNITY edition
* BSS Commerce does not guarantee correct work of this extension
* on any other Magento edition except Magento COMMUNITY edition.
* BSS Commerce does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   BSS
* @package    Bss_DeleteOrder
* @author     Extension Team
* @copyright  Copyright (c) 2014-2105 BSS Commerce Co. ( http://bsscommerce.com )
* @license    http://bsscommerce.com/Bss-Commerce-License.txt
*/
class Bss_DeleteOrder_Adminhtml_Deleteorder_DeleteController extends Mage_Adminhtml_Controller_Action {
	public function orderAction() {
		$ids = $this->getRequest()->getParam('order_ids');
		foreach ($ids as $id) {
			try{
				$order = Mage::getModel('sales/order')->load($id);
				if($order) {
					$order->delete();
					Mage::getModel('deleteorder/delete')->deleteorder($id);
					Mage::getSingleton('adminhtml/session')->addSuccess('Delete success order id '.$id );
				}
			}catch(Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError('Error delete order id '.$id );
				//Mage::logException($e);
			}
		}
		Mage::app()->getResponse()->setRedirect(Mage::helper("adminhtml")->getUrl('adminhtml/sales_order'));
	}

	public function invoiceAction() {
		$ids = $this->getRequest()->getParam('invoice_ids');
		// echo '<pre>',print_r($this->getRequest(),1),'</pre>';exit;
		foreach ($ids as $id) {
			try{
				$invoice = Mage::getModel('sales/order_invoice')->load($id);				
				if($invoice) {
					$orderId = $invoice->getOrder()->getId();
					$order = Mage::getModel('sales/order')->load($orderId);
					// $invoice->cancel()->save();
					$invoiceItems = $invoice->getAllItems();
					$items = $order->getAllVisibleItems();
					foreach($items as $item){
						foreach($invoiceItems as $invoiceItem){
							if($invoiceItem->getOrderItemId() == $item->getItemId()){
								$item->setQtyInvoiced($item->getQtyInvoiced()-$invoiceItem->getQty())
							        ->setTaxInvoiced($item->getTaxInvoiced()-$invoiceItem->getTaxAmount())
							        ->setBaseTaxInvoiced($item->getBaseTaxInvoiced()-$invoiceItem->getBaseTaxAmount())
							        ->setHiddenTaxInvoiced($item->getHiddenTaxInvoiced()-$invoiceItem->getHiddenTaxAmount())
							        ->setBaseHiddenTaxInvoiced($item->getBaseHiddenTaxInvoiced()-$invoiceItem->getBaseHiddenTaxAmount())
							        ->setDiscountInvoiced($item->getDiscountInvoiced()-$invoiceItem->getDiscountAmount())
							        ->setBaseDiscountInvoiced($item->getBaseDiscountInvoiced()-$invoiceItem->getBaseDiscountAmount())
							        ->setRowInvoiced($item->getRowInvoiced()-$invoiceItem->getRowTotal())
							        ->setBaseRowInvoiced($item->getBaseRowInvoiced()-$invoiceItem->getBaseRowTotal());
							}
						}				      	
					}
					$order->setTotalPaid($order->getTotalPaid()-$invoice->getGrandTotal())
			        		->setBaseTotalPaid($order->getBaseTotalPaid()-$invoice->getBaseGrandTotal())
			        		->setTotalInvoiced($order->getTotalInvoiced() - $invoice->getGrandTotal())
			        		->setBaseTotalInvoiced($order->getBaseTotalInvoiced() - $invoice->getBaseGrandTotal())
			         		->setSubtotalInvoiced($order->getSubtotalInvoiced() - $invoice->getSubtotal())
			        		->setBaseSubtotalInvoiced($order->getBaseSubtotalInvoiced() - $invoice->getBaseSubtotal())
			         		->setTaxInvoiced($order->getTaxInvoiced() - $invoice->getTaxAmount())
			         		->setBaseTaxInvoiced($order->getBaseTaxInvoiced() - $invoice->getBaseTaxAmount())
			         		->setHiddenTaxInvoiced($order->getHiddenTaxInvoiced() - $invoice->getHiddenTaxAmount())
			         		->setBaseHiddenTaxInvoiced($order->getBaseHiddenTaxInvoiced() - $invoice->getBaseHiddenTaxAmount())
			         		->setShippingTaxInvoiced($order->getShippingTaxInvoiced() - $invoice->getShippingTaxAmount())
			         		->setBaseShippingTaxInvoiced($order->getBaseShippingTaxInvoiced() - $invoice->getBaseShippingTaxAmount())
			         		->setShippingInvoiced($order->getShippingInvoiced() - $invoice->getShippingAmount())
			        		->setBaseShippingInvoiced($order->getBaseShippingInvoiced() - $invoice->getBaseShippingAmount())
			         		->setDiscountInvoiced($order->getDiscountInvoiced() - $invoice->getDiscountAmount())
			         		->setBaseDiscountInvoiced($order->getBaseDiscountInvoiced() - $invoice->getBaseDiscountAmount())
			         		->setBaseTotalInvoicedCost($order->getBaseTotalInvoicedCost() - $invoice->getBaseCost())
			        		->save()
			        ;
					$invoice->delete();
					Mage::getModel('deleteorder/delete')->deleteother($id,'sales_flat_invoice_grid');
					if($order->hasShipments() || $order->hasInvoices()){
						$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
					}else{
						$order->setState(Mage_Sales_Model_Order::STATE_NEW, true)->save();
					}
					Mage::getSingleton('adminhtml/session')->addSuccess('Delete success invoice id '.$id );
				}
			}catch(Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError('Error delete invoice id '.$id );
				// Mage::logException($e);
			}
		}
		Mage::app()->getResponse()->setRedirect(Mage::helper("adminhtml")->getUrl('adminhtml/sales_invoice'));
	}

	public function cancelinvoiceAction() {
		$ids = $this->getRequest()->getParam('invoice_ids');
		//echo '<pre>',print_r($this->getRequest(),1),'</pre>';exit;
		foreach ($ids as $id) {
			try{
				$invoice = Mage::getModel('sales/order_invoice')->load($id);				
				if($invoice) {
					$orderId = $invoice->getOrder()->getId();
					$order = Mage::getModel('sales/order')->load($orderId);
					$invoice->cancel()->save();					
					Mage::getSingleton('adminhtml/session')->addSuccess('Delete success invoice id '.$id );
				}
			}catch(Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError('Error delete invoice id '.$id );
			}
		}
		Mage::app()->getResponse()->setRedirect(Mage::helper("adminhtml")->getUrl('adminhtml/sales_invoice'));
	}

	public function shipmentAction() {
		$ids = $this->getRequest()->getParam('shipment_ids');
		foreach ($ids as $id) {
			try{
				$shipment = Mage::getModel('sales/order_shipment')->load($id);
				if($shipment) {
					$orderId = $shipment->getOrder()->getId();
					$order = Mage::getModel('sales/order')->load($orderId);
					$shipmentItems = $shipment->getAllItems();
					$items = $order->getAllVisibleItems();
					foreach($items as $item){
						foreach($shipmentItems as $shipmentItem){
							if($shipmentItem->getOrderItemId() == $item->getItemId()){
								$item->setQtyShipped($item->getQtyShipped() - $shipmentItem->getQty());
							}
						}				      	
					}
					$shipment->delete();
					Mage::getModel('deleteorder/delete')->deleteother($id,'sales_flat_shipment_grid');
					if($order->hasInvoices() || $order->hasShipments()){
						$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
					}else{
						$order->setState(Mage_Sales_Model_Order::STATE_NEW, true)->save();
					}
					Mage::getSingleton('adminhtml/session')->addSuccess('Delete success shipment id '.$id );
				}
			
			}catch(Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError('Error delete shipment id '.$id );
				//Mage::logException($e);
			}
		}
		Mage::app()->getResponse()->setRedirect(Mage::helper("adminhtml")->getUrl('adminhtml/sales_shipment'));
	}

	public function creditmemoAction() {
		$ids = $this->getRequest()->getParam('creditmemo_ids');
		foreach ($ids as $id) {
			try{
				$creditmemo = Mage::getModel('sales/order_creditmemo')->load($id);
				if($creditmemo) {
					$orderId = $creditmemo->getOrder()->getId();
					$order = Mage::getModel('sales/order')->load($orderId);
					// $creditmemo->cancel()->save()->getOrder()->save();
					$order->setBaseDiscountRefunded($order->getBaseDiscountRefunded() - $creditmemo->getBaseDiscountAmount())
						->setBaseShippingRefunded($order->getBaseShippingRefunded() - $creditmemo->getBaseShippingAmount())
						->setBaseShippingTaxRefunded($order->getBaseShippingTaxRefunded() - $creditmemo->getBaseShippingTaxAmount())
						->setBaseSubtotalRefunded($order->getBaseSubtotalRefunded() - $creditmemo->getBaseSubtotal())
						->setBaseTaxRefunded($order->getBaseTaxRefunded() - $creditmemo->getBaseTaxAmount())
						->setBaseTotalRefunded($order->getBaseTotalRefunded() - $creditmemo->getBaseGrandTotal())
						->setDiscountRefunded($order->getDiscountRefunded() - $creditmemo->getDiscountAmount())
						->setShippingRefunded($order->getShippingRefunded() - $creditmemo->getShippingAmount())
						->setShippingTaxRefunded($order->getShippingTaxRefunded() - $creditmemo->getShippingTaxAmount())
						->setSubtotalRefunded($order->getSubtotalRefunded() - $creditmemo->getSubtotal())
						->setTaxRefunded($order->getTaxRefunded() - $creditmemo->getTaxAmount())
						->setTotalRefunded($order->getTotalRefunded() - $creditmemo->getGrandTotal())
						->setHiddenTaxRefunded($order->getHiddenTaxRefunded() - $creditmemo->getHiddenTaxAmount())
						->setBaseHiddenTaxRefunded($order->getBaseHiddenTaxRefunded() - $creditmemo->getBaseHiddenTaxAmount());
					if ($creditmemo->getTransactionId()) {
			            $order->setTotalOnlineRefunded(
			                $order->getTotalOnlineRefunded()-$creditmemo->getGrandTotal()
			            );
			            $order->setBaseTotalOnlineRefunded(
			                $order->getBaseTotalOnlineRefunded()-$creditmemo->getBaseGrandTotal()
			            );
			        }
			        else {
			            $order->setTotalOfflineRefunded(
			                $order->getTotalOfflineRefunded()-$creditmemo->getGrandTotal()
			            );
			            $order->setBaseTotalOfflineRefunded(
			                $order->getBaseTotalOfflineRefunded()-$creditmemo->getBaseGrandTotal()
			            );
			        }
					$items = $order->getAllVisibleItems();
					$creditmemoItems = $creditmemo->getAllItems();
					foreach($items as $item){
						foreach($creditmemoItems as $creditmemoItem){
							if($creditmemoItem->getOrderItemId() == $item->getItemId()){
								$item->setQtyRefunded($item->getQtyRefunded() - $creditmemoItem->getQty())
						      		->setAmountRefunded($item->getAmountRefunded() - $creditmemoItem->getRowTotal())
						      		->setBaseAmountRefunded($item->getBaseAmountRefunded() - $creditmemoItem->getBaseRowTotal())

						      		->setHiddenTaxRefunded($item->getHiddenTaxRefunded() - $creditmemoItem->getHiddenTaxAmount())
						      		->setBaseHiddenTaxRefunded($item->getBaseHiddenTaxRefunded() - $creditmemoItem->getBaseHiddenTaxAmount())
						      		->setTaxRefunded($item->getTaxRefunded() - $creditmemoItem->getTaxAmount())
						      		->setBaseTaxRefunded($item->getBaseTaxRefunded() - $creditmemoItem->getBaseTaxAmount())
						      		->setDiscountRefunded($item->getDiscountRefunded() - $creditmemoItem->getDiscountAmount())
						      		->setBaseDiscountRefunded($item->getBaseDiscountRefunded() - $creditmemoItem->getBaseDiscountAmount());
							}
						}	
					}
					$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
					$creditmemo->delete();
					Mage::getModel('deleteorder/delete')->deleteother($id,'sales_flat_creditmemo_grid');
					Mage::getSingleton('adminhtml/session')->addSuccess('Delete success credit memo id '.$id );
				}
			}catch(Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError('Error delete credit memo id '.$id );
				Mage::logException($e);
			}
		}
		Mage::app()->getResponse()->setRedirect(Mage::helper("adminhtml")->getUrl('adminhtml/sales_creditmemo'));
	}
}