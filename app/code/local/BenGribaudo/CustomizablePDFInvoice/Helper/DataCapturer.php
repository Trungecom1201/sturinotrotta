<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * If this class is deemed a derivative  work (as it extends a OSL-licensed Magento class 
  * [Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)]), then it is released under 
  * the Open Software License ("OSL") v. 3.0 <http://www.opensource.org/licenses/osl-3.0.php>. 
  * Otherwise, its use is governed  by the terms of the commercial license agreement you received 
  * when you purchased a license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Helper_DataCapturer extends Mage_Sales_Model_Order_Pdf_Abstract {
	private $draw;
	
	public function __construct() {
		$this->_initRenderer('invoice');
	}
	
	public function drawLineBlocks(Zend_Pdf_Page $page, array $draw, array $pageSettings = array()) {
		$this->draw = $draw;
		return $page;
	}
	
	public function getRenderer($type)
	{
		return  parent::getRenderer($type);
	}
	
	public function insertTotals($a, $b) { return parent::insertTotals($a, $b); }
	public function _getDraw() { return $this->draw; }
	public function getPdf() {}
}