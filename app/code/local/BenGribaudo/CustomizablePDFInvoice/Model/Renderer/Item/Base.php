<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
abstract class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Base extends BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Base {
	protected $data, $defaultTextStyle = 'Item';
	private $mappings = array(
			'product_name' => 'ProductName', 
			'sku' => 'Sku',
			'price' => 'Price',
			'quantity' => 'Quantity',
			'tax' => 'Tax',
			'subtotal' => 'Subtotal'
		);

	protected function Mapper() {
		$content = array();
		
		foreach ($this->mappings as $k => $v) {
			$isVisableMethod = "getColumn{$v}Visable";
			if ($this->settings->$isVisableMethod()) {
				$widthMethod = "getColumn{$v}Width";
				$alignMethod = "getColumn{$v}Align";
				if (count($content) == 0 && $this->settings->$alignMethod() == BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer::AlignLeft) {
					$noLeftPadding = true;
				} else {
					$noLeftPadding = false;
				}
				$value = (array_key_exists($k, $this->data))? $this->data[$k] : null;
				$content[] = $this->CreateColumn($value, $this->settings->$widthMethod(), $this->settings->$alignMethod(), $noLeftPadding);
			}
		}
		
		$content[0]->setMargin($this->margin(0, 0, 2, 0));

		return $content;
	}
	
	protected function CreateColumn($content, $width, $align = BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_AlignContainer::AlignRight, $noLeftPadding = false) {
		$content = (is_object($content))? $content : $this->text($content, $this->defaultTextStyle);
		$leftPadding = ($noLeftPadding)? 0 :  2;
		return $this->content("AlignContainer", $content)
				->setAlign($align)
				->setMargin($this->margin($leftPadding, 0, 2, 0))
				->setWidth($width);
	}
	
	protected function content($type, $arg = null) {
		$className = "BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_" . $type;
		return new $className($arg);
	}
}