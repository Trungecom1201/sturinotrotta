<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Adapter_ItemDataCapturers_Bundle extends BenGribaudo_CustomizablePDFInvoice_Model_Adapter_ItemDataCapturers_Default {
	public function __construct() {
		$this->itemMapping[35] = 'grouping';
		$this->itemMapping[40] = 'product_name';
	}
		
	public function adapt($data) {
		$output = array();
	
		foreach ($data['lines'] as $item) {
			$output[] = $this->mapItem($item);
		}

		return $output;
	}
	
}