<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_BoxContent extends BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	private $borderThickness, $backgroundColor, $lineColor, $padding;
	
	public static function create($content) {
		return new BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_BoxContent($content);
	}
	
	public function setBorderThickness($thickness) {
		$this->borderThickness = $thickness;
		return $this;
	}
	
	public function setPadding($padding) {
		$this->padding = $padding;
		return $this;
	}
	
	public function setBorderColor($color) {
		$this->lineColor = $color;
		return $this;
	}
	
	public function setBackgroundColor($color) {
		$this->backgroundColor = $color;
		return $this;
	}
	
	protected function getContentHeight() {
		$output = $this->content->getHeight() + ($this->borderThickness * 2);
		if ($this->padding) {
			$output += $this->padding->getTop() + $this->padding->getBottom();
		}
		return $output;
	}
	
	protected function RenderContent($page, $xStart, $xEnd, $y) {
		$backgroundColor = ($this->backgroundColor != null)? $this->backgroundColor : new Zend_Pdf_Color_Rgb(1, 1, 1);
		$borderThickness = ($this->borderThickness != null)? $this->borderThickness : 0;
		$halfThickness = $borderThickness / 2;
		
		$page->saveGS();
		$page->setFillColor($backgroundColor);
		
		if ($this->lineColor != null) {
			$page->setLineColor($this->lineColor);
		}

		$page->setLineWidth($borderThickness);
		$rectangleYEnd = round($y - $this->content->getHeight() - ($halfThickness * 2));
		if ($this->padding) {
			$rectangleYEnd -= $this->padding->getTop() + $this->padding->getBottom();
		}
		$page->drawRectangle(round($xStart + $halfThickness), round($y - $halfThickness), round($xEnd - $halfThickness), $rectangleYEnd);
		
		$page->restoreGS();
		
		$contentXStart = $xStart + $borderThickness;
		$contentXEnd = $xEnd - $borderThickness;
		$contentYStart = $y - $borderThickness;
		
		if ($this->padding) {
			$contentXStart += $this->padding->getLeft();
			$contentXEnd -= $this->padding->getRight();
			$contentYStart -= $this->padding->getTop();
		}
	
		$this->content->Render($page, $contentXStart, $contentXEnd, $contentYStart);
	}
}