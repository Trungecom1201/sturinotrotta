<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_ImageContent extends BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	public static function create($content) {
		return new BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_ImageContent($content);
	}
	public function __construct($imagePath) { 
		parent::__construct(null);
		$this->content = Zend_Pdf_Image::imageWithPath($imagePath);
	}
	protected function getContentWidth() {
		return $this->content->getPixelWidth();
	}
	protected function getContentHeight() {
		return $this->content->getPixelHeight();
	}
	protected function RenderContent($page, $xStart, $xEnd, $y) {
		$page->drawImage($this->content, $xStart, $y - $this->getHeight(), $xEnd, $y);
	}
	
}
