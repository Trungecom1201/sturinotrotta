<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Invoice extends BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Base {
	private $invoice, $pageMargin, $pageContent, $pageContainer, $pageContainers = array(), $itemAreaContainer;

	public function __construct($invoice) {
	
		parent::__construct(new BenGribaudo_CustomizablePDFInvoice_Helper_Settings());
		$this->invoice = $invoice;
		$margin = $this->settings->getPageMargin();
		$this->pageMargin = $this->margin($margin, $margin, $margin, $margin);
		
		$this->newPage();
	}
	
	public static function RenderInvoices($invoices) {
		$output = array();
		foreach ($invoices as $invoice) {
			$renderer = new BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Invoice($invoice);
			$output = array_merge($output, $renderer->Render());
		}
		return $output;
	}
	
	public function Render() {
		$this->assemble();
		return $this->outputPdf();
	}
		
	private function assemble() {
		$this->placeContent(BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Header::RenderContent($this->invoice, $this->settings));
		$this->placeContent($this->itemsHeader());

		$itemRenderer = new BenGribaudo_CustomizablePDFInvoice_Model_Renderer_ItemRow($this->settings);
		foreach ($this->invoice['items'] as $item) {
			$this->placeItemAreaContent($itemRenderer->Render($item));
		}

		$this->placeItemAreaContent(BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Totals::RenderContent($this->invoice, $this->settings), false);
	}
	
	private function placeItemAreaContent($item, $header = true) {
		if ($this->itemAreaContainer != null && $this->spaceAvailable($item)) {
			$this->placeContent($item, "itemsHeader", $this->itemAreaContainer);
		} else {
			$container = $this->content('VerticalContainer', array($item))
				->setMargin($this->margin(10, 0, 10, 0));
			$headerCallback = ($header)? "itemsHeader" : null;
			$this->placeContent($container, $headerCallback);
			$this->itemAreaContainer = $container;
		}
	}
	
	private function itemsHeader() {
		return BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Header::RenderContent($this->invoice, $this->settings);
	}
	
	private function spaceAvailable($content) {
		return ($content->getHeight() + $this->pageContainer->getHeight() <= $this->settings->getPageHeight());
	}
	
	private function placeContent($content, $headerFunction = null, &$container = null) {
		if (!$this->spaceAvailable($content)) {
			$this->newPage();
			if ($headerFunction) { $this->pageContent[] = $this->$headerFunction(); }
		} 
		if ($container) {
			$containerContent = &$container->getContent();
			$containerContent[] = $content;
		} else {
			$this->pageContent[] = $content;
		
		}
	}
	
	private function outputPdf() {
		$output = array();
	
		foreach ($this->pageContainers as $container) {
			$this->page = new Zend_Pdf_Page($this->settings->getPageWidth(), $this->settings->getPageHeight());
			$output[] = $this->page;
			$container->Render($this->page, 0, $this->settings->getPageWidth(), $this->settings->getPageHeight());
		}
		
		return $output;
	}
	
	private function newPage() {
		$this->pageContainer = $this->content('VerticalContainer', array())
			->setMargin($this->pageMargin);
		$this->pageContainers[] = $this->pageContainer;
		$this->pageContent = &$this->pageContainer->getContent();
	}
}