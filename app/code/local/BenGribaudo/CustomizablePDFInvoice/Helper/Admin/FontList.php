<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Helper_Admin_FontList
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'Courier', 'label'=>Mage::helper('adminhtml')->__('Courier')),
            array('value' => 'Courier-Bold', 'label'=>Mage::helper('adminhtml')->__('Courier-Bold')),
	    array('value' => 'Courier-Oblique', 'label'=>Mage::helper('adminhtml')->__('Courier-Oblique')),
	    array('value' => 'Courier-BoldOblique', 'label'=>Mage::helper('adminhtml')->__('Courier-Bold Oblique')),
	    array('value' => 'Helvetica', 'label'=>Mage::helper('adminhtml')->__('Helvetica')),
	    array('value' => 'Helvetica-Bold', 'label'=>Mage::helper('adminhtml')->__('Helvetica-Bold')),
	    array('value' => 'Helvetica-Oblique', 'label'=>Mage::helper('adminhtml')->__('Helvetica-Oblique')),
	    array('value' => 'Helvetica-BoldOblique', 'label'=>Mage::helper('adminhtml')->__('Helvetica-Bold Oblique')),
	    array('value' => 'Symbol', 'label'=>Mage::helper('adminhtml')->__('Symbol')),
	    array('value' => 'Times-Roman', 'label'=>Mage::helper('adminhtml')->__('Times-Roman')),
	    array('value' => 'Times-Bold', 'label'=>Mage::helper('adminhtml')->__('Times-Bold')),
	    array('value' => 'Times-Italic', 'label'=>Mage::helper('adminhtml')->__('Times-Italic')),
	    array('value' => 'Times-BoldItalic', 'label'=>Mage::helper('adminhtml')->__('Times-Bold Italic')),
	    array('value' => 'ZapfDingbats', 'label'=>Mage::helper('adminhtml')->__('Zapf Dingbats')),
	    array('value' => 'Libertine', 'label'=>Mage::helper('adminhtml')->__('Libertine (from Magento)')),
	    array('value' => 'Libertine-Bold', 'label'=>Mage::helper('adminhtml')->__('Libertine-Bold (from Magento)')),
	    array('value' => 'Libertine-Italic', 'label'=>Mage::helper('adminhtml')->__('Libertine-Italic (from Magento)'))
        );
    }

}