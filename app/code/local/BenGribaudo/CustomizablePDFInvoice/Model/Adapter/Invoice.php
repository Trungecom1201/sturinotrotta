<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Adapter_Invoice extends BenGribaudo_CustomizablePDFInvoice_Model_Adapter_Abstract {
	private $store;
	
	public function __construct($invoice) {
		$this->invoice = $invoice;
		$this->order = $this->invoice->getOrder();
		$this->store = $this->invoice->getStore();
	}
	
	public static function AdaptInvoices($invoices) {
		$output = array();
		foreach ($invoices as $invoice) {	
			$adapter = new BenGribaudo_CustomizablePDFInvoice_Model_Adapter_Invoice($invoice);
			$output[] = $adapter->adapt();
		}
		return $output;
	}

	public function adapt() {
		$this->preAdapt();
		$rendererHelper = new BenGribaudo_CustomizablePDFInvoice_Helper_MagentoPdf();
		
		$labelMappings = array(
			'order_date' => 'Order # ',
			'order_date' => 'Order Date: ',
			'sold_to' => 'SOLD TO:',
			'ship_to' => 'SHIP TO:',
			'payment_method' => 'Payment Method:',
			'invoice_id' => 'Invoice # ',
			'shipping_method' => 'Shipping Method:',
			'total_shipping_charges' => 'Total Shipping Charges',
			'order_id' => 'Order # ',
			'item_columns' => array( 
				'product_name' => 'Products',
				'sku' => 'SKU',
				'price' => 'Price',
				'quantity' => 'Qty',
				'tax' => 'Tax',
				'subtotal' => 'Subtotal'
			)
		);
		$valueMappings = array(
			'store_address' => Mage::getStoreConfig('sales/identity/address', $this->store),
			'image_path' => $this->logo($this->store),
			'display_order_id' => Mage::getStoreConfigFlag(Mage_Sales_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID, $this->store),
			'order_id' => $this->order->getRealOrderId(),
			'order_date' => Mage::helper('core')->formatDate($this->order->getCreatedAtStoreDate(), 'medium', false),
			'billing_address' => $this->cleanUpText($rendererHelper->formatAddress($this->order->getBillingAddress()->format('pdf'))),
			'payments' => $this->paymentInformation(),
			'invoice_id' => $this->invoice->getIncrementId(),
			'items' => BenGribaudo_CustomizablePDFInvoice_Model_Adapter_Item::AdaptItems($this->invoice),
			'totals' => $this->totals(),
			'virtual' => $this->order->getIsVirtual(),
			'footer_visable' => Mage::getStoreConfigFlag('sales_pdf/bengribaudo_customizableinvoice/footer_visable', $this->store),
			'footer_text' => Mage::getStoreConfig('sales_pdf/bengribaudo_customizableinvoice/footer_text', $this->store)
		);

		if (!$this->order->getIsVirtual()) {
			$valueMappings += array(
				'shipping_method' => $this->order->getShippingDescription(),
				'shipping_address' => $this->cleanUpText($rendererHelper->formatAddress($this->order->getShippingAddress()->format('pdf'))),
				'total_shipping_charges' => $this->order->formatPriceTxt($this->order->getShippingAmount())
			);
		}
		
		$data = $this->map($valueMappings);
		$data['labels'] = $this->map($labelMappings, true);
				
		$this->postAdapt();
		return $data;
	}
	
	private function totals() {
		$dc = new BenGribaudo_CustomizablePDFInvoice_Helper_DataCapturer();
		$dc->insertTotals(new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4), $this->invoice);
		$totals = $dc->_getDraw();
		
		foreach ($totals[0]['lines'] as $total) {
					$output[] = array(
						'label' => $total[0]['text'], 
						'amount' => $total[1]['text']
					);
		}
		
		return $output;
	}
	
	private function map($mappings, $translate = false) {
		$output = array();
		foreach ($mappings as $k => $v) {
			$preOutput = (is_array($v))? $this->map($v, $translate) : (($translate) ? $this->__($v) : $v);
			$output[$k] = str_replace("\r", '', $preOutput);
		}
		return $output;
	}
	
	private function __($input) {
		return Mage::helper('sales')->__($input);
	}
	
	private function cleanUpText($text) {
		$cleanUpFunction = create_function('$a, $b', 'return (($a !== null)? $a . "\n" : null) . trim(strip_tags($b));');
		return array_reduce($text, $cleanUpFunction); 
	}
	
	private function logo() {
		$basePath = Mage::getStoreConfig('sales_pdf/bengribaudo_customizableinvoice/store_info_logo', $this->store);
		$mediaFileStorePath = Mage::getStoreConfig('system/filesystem/media', $this->store);
		
		if ($basePath) {
			$fullPath =  $mediaFileStorePath . '/bengribaudo/customizablepdfinvoice/store_info_logo/' . $basePath;
		} else {
			$basePath = Mage::getStoreConfig('sales/identity/logo', $this->store);
			$fullPath = $mediaFileStorePath . '/sales/store/logo/' . $basePath;
		}
		
		if ($fullPath && is_file($fullPath)) {
			return $fullPath;
		}
	}
	
	private function paymentInformation() {
		$cleanUpFunction = create_function('$value', 'return (trim(strip_tags($value)) != "");');
		
		$payment = Mage::helper('payment')->getInfoBlock($this->order->getPayment())
			->setIsSecureMode(true)
			->toPdf();
		$payment = array_filter(explode('{{pdf_row_separator}}', $payment), $cleanUpFunction);
		return $this->cleanUpText($payment);
	}
}