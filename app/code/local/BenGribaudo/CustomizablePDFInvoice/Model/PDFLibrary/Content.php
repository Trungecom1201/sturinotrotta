<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
abstract class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Content {
	protected $content;
	private $margin;
	
	public function __construct($content = null) {
		$this->content = $content;
	}
	
	public function &getContent() {
		return $this->content;
	}
	
	public function setContent($content) {
		$this->content = $content;
		return $this;
	}
	
	public function getWidth() {
		return $this->getContentWidth();
	}
	
	protected function getContentWidth() {
		return null;
	}
	
	protected abstract function getContentHeight() ;
	
	public function getHeight() {
		if ($this->getContentHeight() == null) {
			return null;
		}
		
		$addTop = ($this->margin != null && $this->margin->getTop() > 0)? $this->margin->getTop() : 0;
		$addBottom = ($this->margin != null && $this->margin->getBottom() > 0)? $this->margin->getBottom() : 0;

		return $this->getContentHeight() + $addTop + $addBottom;
	}
	
	protected abstract function RenderContent($page, $xStart, $xEnd, $y);
	
	public function Render($page, $xStart, $xEnd, $y) {
		$leftX = ($this->margin != null)? $xStart + $this->margin->getLeft() : $xStart;
		$rightX = ($this->margin != null)? $xEnd - $this->margin->getRight() : $xEnd;
		$topY = ($this->margin != null)? $y - $this->margin->getTop() : $y;
		
		$this->RenderContent($page, $leftX, $rightX, $topY);
	}
	
	public function setMargin($margin) {
		$this->margin = $margin;
		return $this;
	}
}