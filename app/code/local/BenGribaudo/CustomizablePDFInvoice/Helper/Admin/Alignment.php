<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Helper_Admin_Alignment
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'Left', 'label'=>Mage::helper('adminhtml')->__('Left')),
	    array('value' => 'Center', 'label'=>Mage::helper('adminhtml')->__('Center')),
            array('value' => 'Right', 'label'=>Mage::helper('adminhtml')->__('Right'))
        );
    }

}