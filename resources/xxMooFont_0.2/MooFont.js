window.addEvent('domready', function() { 
	
	if(Browser.Engine.webkit ){return;}
	var h1 =new MooFont({ selector:'h1'});
	var h2 =new MooFont({ selector:'h2'});	
	var h3Links = new MooFont({ 
		selector:'h3 a',
		size:'16', 
		color:'#ffffff',
		bgColor:'#000000',
		color_hover:'#FEFE82',
		bgColor_hover:'#000000'
	});
	var h4 =new MooFont({ selector:'h4'});
	var h5 =new MooFont({ selector:'h5'});
	var h6 =new MooFont({ selector:'h6'});
});


/*
if(typeof sIFR == "function"){
    sIFR.replaceElement(named({sSelector:"body h5", sFlashSrc:"resources/Sheer_Elegance.swf", sColor:"#464227", sWmode:"transparent", sLinkColor:"#464227", sBgColor:"#FFFFFF", sHoverColor:"#464227", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=right&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"body h4", sFlashSrc:"resources/BankGothic_Lt_BT.swf", sColor:"#464227", sWmode:"transparent", sLinkColor:"#464227", sBgColor:"#FFFFFF", sHoverColor:"#464227", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"body h3", sFlashSrc:"resources/BankGothic_Lt_BT.swf", sColor:"#FFFFFF", sWmode:"transparent", sLinkColor:"#FFFFFF", sBgColor:"#FFFFFF", sHoverColor:"#FEFE82", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=center&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"body h2", sFlashSrc:"resources/BankGothic_Lt_BT.swf", sColor:"#464227", sWmode:"transparent", sLinkColor:"#464227", sBgColor:"#FFFFFF", sHoverColor:"#464227", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=center&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"body h1", sFlashSrc:"resources/Sheer_Elegance.swf", sColor:"#464227", sWmode:"transparent", sLinkColor:"#464227", sBgColor:"#FFFFFF", sHoverColor:"#464227", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));
};
*/


/**
 * MooFont 
 * @version 0.2 (beta)
 * @author Jason J. Jaeger | greengeckodesign.com
 * @copyright 2008 Jason John Jaeger
 * @license MIT-style License
 *			Permission is hereby granted, free of charge, to any person obtaining a copy
 *			of this software and associated documentation files (the "Software"), to deal
 *			in the Software without restriction, including without limitation the rights
 *			to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *			copies of the Software, and to permit persons to whom the Software is
 *			furnished to do so, subject to the following conditions:
 *	
 *			The above copyright notice and this permission notice shall be included in
 *			all copies or substantial portions of the Software.
 *	
 *			THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *			IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *			FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *			AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *			LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *			OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *			THE SOFTWARE.
 *	
 *  @changeLog_________________________________________________________________________________
 *  
 *  May 10th 2008:
 *  JJJ - Incremented version to 0.2
 *  	- Cleaned string before splitting it.
 **/

var MooFont = new Class({
	options: {
        selector:'',
		font:'auto',
		size:'auto',
		color:'auto',
		bgColor:'auto',
		color_hover:'auto',
		bgColor_hover:'auto',
		phpFile:'resources/MooFont_0.2/fontReplacement.php',
		fontDirectory:'fonts/',//relative to the php File
		fontExt:'.ttf'
    },
	
	elements:[],
	transparentBG:'true',
	
	initialize: function(options){
	    this.setOptions(options);
		
		this.elements = $(document.body).getElements(this.options.selector);
		this.elements.each(function(item,index){
			if(!$(item).get('text')){return;}
			var textArr = $(item).get('text').clean().split(" ");
			
			$(item).set('text','');
			textArr.each(function(item2,index2){
				//add a space to the end
				if(item2 != textArr.getLast()){	item2 = item2+' ';	}
				var bgColor = this.options.bgColor;
				if(bgColor === 'auto'){	bgColor = item.getStyle('background-color') ;	}
				if(bgColor === 'transparent'){	bgColor = '#fff';	}
				
				var color = this.options.color;
				if(color === 'auto'){	color = item.getStyle('color') ; }
				
				var size = this.options.size;
				if(size === 'auto'){	size = item.getStyle('font-size'); }
				
				var font = this.options.font;
				if(font === 'auto'){
					font= item.getStyle('font-family').split(",")[0].replace('"','').replace('"','').trim();
				}
				
				var color_hover = this.options.color_hover;
				var bgColor_hover = this.options.bgColor_hover;
				
				var fontImg  = new Element('img');

				fontImg.store('text', escape(item2));
				fontImg.store('font', escape(this.options.fontDirectory + font + this.options.fontExt));
				fontImg.store('size', escape(size));
				fontImg.store('color', escape(color));
				fontImg.store('color_hover', escape(color_hover));
				fontImg.store('bgColor', escape(bgColor));
				fontImg.store('bgColor_hover', escape(bgColor_hover));
				fontImg.store('trans', escape(this.transparentBG));
				
				var imgSrc = this.options.phpFile+
					'?text='+fontImg.retrieve('text')+
					'&font='+fontImg.retrieve('font')+
					'&size='+fontImg.retrieve('size')+
					'&color='+fontImg.retrieve('color')+
					'&bgColor='+fontImg.retrieve('bgColor')+
					'&trans='+fontImg.retrieve('trans');

				fontImg.set('src',imgSrc);
				fontImg.inject(item,'inside');	

			}.bind(this));
			
			item.addEvents({
					'mouseenter': function(e){
						item.getElements('img').each(function(item3,index3){
							
							if(item3.retrieve('color_hover') === 'auto'){ 
								item3.store('color_hover', escape(item.getStyle('color')));	
							}
							if(item3.retrieve('bgColor_hover') === 'auto'){	
								item3.store('bgColor_hover', escape(item.getStyle('background-color')));	
							}
							if(item3.retrieve('bgColor_hover') === 'transparent'){	
								item3.store('bgColor_hover', escape('#ffffff'));	
							}
							
							var imgSrc = this.options.phpFile+
								'?text='+item3.retrieve('text')+
								'&font='+item3.retrieve('font')+
								'&size='+item3.retrieve('size')+
								'&color='+item3.retrieve('color_hover')+
								'&bgColor='+item3.retrieve('bgColor_hover')+
								'&trans='+item3.retrieve('trans');
							item3.set('src',imgSrc);
						}.bind(this));	
					}.bind(this),
					
					'mouseleave': function(e){
						item.getElements('img').each(function(item3,index3){
							var imgSrc = this.options.phpFile+
								'?text='+item3.retrieve('text')+
								'&font='+item3.retrieve('font')+
								'&size='+item3.retrieve('size')+
								'&color='+item3.retrieve('color')+
								'&bgColor='+item3.retrieve('bgColor')+
								'&trans='+item3.retrieve('trans');
							item3.set('src',imgSrc);
						}.bind(this));		
					}.bind(this)
				});	
			
		}.bind(this));

		
    }

});
MooFont.implement(new Options);
MooFont.implement(new Events);