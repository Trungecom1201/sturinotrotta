<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Margin {
	private $left, $top, $right, $bottom;
	public function __construct($left = 0, $top = 0, $right = 0, $bottom = 0) {
		$this->left = $left;
		$this->top = $top;
		$this->right = $right;
		$this->bottom = $bottom;
	}
	public function getLeft() { return $this->left; }
	public function getRight() { return $this->right; }
	public function getTop() { return $this->top; }
	public function getBottom() { return $this->bottom; }
}