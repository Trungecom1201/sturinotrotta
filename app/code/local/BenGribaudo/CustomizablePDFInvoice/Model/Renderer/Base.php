<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
abstract class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Base {
	protected $settings, $styles = array();
	
	public function __construct($settings) {
		$this->settings = $settings;
	}
	
	public abstract function Render();

	protected function content($type, $arg = null) {
		$className = "BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_" . $type;
		return new $className($arg);
	}
	
	protected function margin($left, $top, $right, $bottom) {
		return new BenGribaudo_CustomizablePDFInvoice_Model_PDFLibrary_Margin($left, $top, $right, $bottom);
	}
	
	protected function text($text, $styleName) {
		$style = (array_key_exists($styleName, $this->styles))? $this->styles[$styleName] : array();

		if (!array_key_exists('font', $style)) {
			$methodName = 'get' . $styleName . 'Font';
			$style['font'] = $this->settings->$methodName();
		}
		if (!array_key_exists('font_size', $style)) {
			$methodName = 'get' . $styleName . 'FontSize';
			$style['font_size'] = $this->settings->$methodName();
		}
		
		
		$output = $this->content('TextContent', $text)
				->setFont($style['font'])
				->setFontSize($style['font_size']);
		
		if (array_key_exists('margin', $style)) {
			$output->setMargin($style['margin']);
		}
		return $output;
	}
}