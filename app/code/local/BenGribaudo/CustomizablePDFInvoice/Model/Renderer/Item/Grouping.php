<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Grouping extends BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Item_Default {
	protected $productNameKey = 'grouping', $productNameStyle = 'ItemGrouping';
}