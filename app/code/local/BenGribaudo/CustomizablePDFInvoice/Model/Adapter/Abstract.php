<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
abstract class BenGribaudo_CustomizablePDFInvoice_Model_Adapter_Abstract {
	protected $invoice, $order;
	
	protected function preAdapt() {
		if ($this->invoice->getStoreId()) {
			Mage::app()->getLocale()->emulate($this->invoice->getStoreId());
			Mage::app()->setCurrentStore($this->invoice->getStoreId());
		}	
	}
	
	protected function postAdapt() {
		if ($this->invoice->getStoreId()) {
			Mage::app()->getLocale()->revert();
		}
	}
}