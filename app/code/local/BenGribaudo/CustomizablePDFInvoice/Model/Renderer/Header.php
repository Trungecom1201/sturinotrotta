<?php
/*
  * Copyright 2011 Ben Gribaudo, LLC. All Rights Reserved
  *
  * This is commercial software. Use of this software is governed  by the 
  * terms of the commercial license agreement you received when you 
  * purchased your license to use this module.  
  */
class BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Header extends BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Base {
	private $invoice, $marginLeft;
	
	public static function RenderContent($invoice, $settings) {
		$renderer = new BenGribaudo_CustomizablePDFInvoice_Model_Renderer_Header($invoice, $settings);
		return $renderer->Render();
	}

	public function __construct($invoice, $settings) {
		parent::__construct($settings);
		$this->invoice = $invoice;
		$this->marginLeft = 10;
		$this->styles = array(
			'BoxHeader' => array(
				'margin' => $this->margin(10, 10, 10, 10)
			),
			'BoxContent' => array(
				'margin' => $this->margin(10, 5, 10, 5)
			)
		);
	}

	public function Render() {
		$labels = $this->invoice['labels'];
		$content = array();
	
		$invoiceHeaderTextMargin = $this->margin($this->marginLeft, 5, 5, 5);
		$storeInfoContent = array();
		if ($this->invoice['image_path']) {
			$storeInfoContent[] = $this->content('ImageContent', $this->invoice['image_path']);
		}
		
		$storeAddress = preg_split("/(\r\n|\n|\r)/", $this->invoice['store_address']);
		$storeAddress_FirstLine = array_shift($storeAddress);
		$storeAddress_Remainder = implode("\n", $storeAddress);
		$storeInfoContent[] = $this->content('VerticalContainer', array(
			$this->text($storeAddress_FirstLine, 'StoreInfoFirstLine'), 
			$this->text($storeAddress_Remainder, 'StoreInfo')
		))->setMargin($this->margin($this->marginLeft, $this->settings->getStoreInfoTopMargin(), 0, 0));
		
		$content[] = $this->content('HorizontalContainer', $storeInfoContent)
						->setMargin($this->margin(0, 0, 0, 5));
						
		$invoiceInfoString = $labels['invoice_id'] . $this->invoice['invoice_id'];
		if ($this->invoice['display_order_id']) {
			$invoiceInfoString .= "\n" . $labels['order_id'] . $this->invoice['order_id'];
		}		
		$invoiceInfoString .= "\n" . $labels['order_date'] . $this->invoice['order_date'];

		$content[] = $this->content('BoxContent', $this->text($invoiceInfoString, 'InvoiceInfo'))
						->setBackgroundColor($this->settings->getTitleBoxBackgroundColor())
						->setBorderColor($this->settings->getBorderColor())
						->setPadding($invoiceHeaderTextMargin);
		
		$topRightBoxHeading = ($this->invoice['virtual'])? $labels['payment_method'] : $labels['ship_to'];
		$topRightBoxContent = ($this->invoice['virtual'])? $this->invoice['payments'] : $this->invoice['shipping_address'];
		$content[] = $this->boxRow($labels['sold_to'], $this->invoice['billing_address'], $topRightBoxHeading, $topRightBoxContent);
		
		if ($this->invoice['virtual'] == false) {
			$shipmentInfo = $this->invoice['shipping_method'] . "\n(" . $labels['total_shipping_charges'] . " " . $this->invoice['total_shipping_charges'] . ")";
			$content[] = $this->boxRow($labels['payment_method'], $this->invoice['payments'], $labels['shipping_method'], $shipmentInfo);
		}
		
		return $this->content('VerticalContainer', $content)
			->setMargin($this->margin(0, 0, 0, 15));
	}
	
	private function boxRow($leftHeading, $leftContent, $rightHeading, $rightContent) {
		$titleBoxMargin = $this->margin($this->marginLeft, 10, 10, 10);
		$contentBoxMargin = $this->margin($this->marginLeft, 5, 10, 5);
		$boxMakeLeftOverlapMargin = $this->margin(-1, 0, 0, 0);
		$boxMakeTopOverlapMargin = $this->margin(0, -1, 0, 0);
	
		$content = array();
		$content[] = $this->content('HorizontalContainer', array(
			$this->content('BoxContent', $this->text($leftHeading, 'BoxHeader'))
				->setBorderColor($this->settings->getBorderColor())
				->setBackgroundColor($this->settings->getTitleBoxBackgroundColor()),
			$this->content('BoxContent', $this->text($rightHeading, 'BoxHeader'))
				->setBorderColor($this->settings->getBorderColor())
				->setBackgroundColor($this->settings->getTitleBoxBackgroundColor())
				->setMargin($boxMakeLeftOverlapMargin)
		));
		$content[] = $this->content('BoxContent', 
			$this->content('HorizontalContainer', array(
				$this->content('AlignContainer', $this->text($leftContent, 'BoxContent')),
				$this->content('AlignContainer', $this->text($rightContent, 'BoxContent'))
			))
		)->setMargin($boxMakeTopOverlapMargin);
		
		return $this->content('VerticalContainer', $content)
				->setMargin($this->margin(0, 5, 0, 0));
	}
}
